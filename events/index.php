<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Слово Делу");
$APPLICATION->SetTitle("Мероприятия"); ?>
<?

$elementCode = arr::_($_GET, 'element');
if ($elementCode && $_p = strpos($elementCode, '/'))
	$elementCode = substr($elementCode, 0, $_p);

if (!$elementCode):
	$APPLICATION->IncludeComponent('newsite:ibface', 'events',
		[
			'DATA_TYPE' => 'ELEMENTS',
			'DATA_IBLOCK_ID' => Site::get()->scheme->events
		]);
else:
	$APPLICATION->IncludeComponent('newsite:events', '', [
		'EVENT_CODE' => $elementCode
	]);
endif;
?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>


