<?
define('UI_NOWRAP', 1);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Слово Делу — Главная");
?>
	<div class="container">
		<? $APPLICATION->IncludeComponent(
			"bitrix:advertising.banner",
			"frontpage",
			array(
				"TYPE" => "FRONTPAGE",
				"CACHE_TYPE" => "A",
				"NOINDEX" => "N",
				"CACHE_TIME" => "3600",
				"COMPONENT_TEMPLATE" => "frontpage",
				"QUANTITY" => "5"
			),
			false
		); ?>
	</div>
	<div class="container has-feedback">
		<div class="question-left-box">
			<? $APPLICATION->IncludeComponent(
				"newsite:ibface",
				"front.cells.experts",
				array(
					'DATA_TYPE' => 'SECTIONS',
					'DATA_IBLOCK_ID' => Site::get()->scheme->questions,
					'SORT_ASC' => true
				),
				false
			); ?>
		</div>
	</div>
<? $APPLICATION->IncludeComponent(
	"bitrix:advertising.banner",
	"raw",
	array(
		"TYPE" => "FRONTPAGE_BOTTOM",
		"CACHE_TYPE" => "A",
		"NOINDEX" => "N",
		"CACHE_TIME" => "3600",
		"QUANTITY" => "1"
	),
	false
); ?>
	<div class="news-box">
		<div class="container">
			<? $APPLICATION->IncludeComponent(
				"newsite:ibface",
				"news.latest",
				array(
					'DATA_TYPE' => 'ELEMENTS',
					'DATA_IBLOCK_ID' => Site::get()->scheme->publications,
					'DATA_FETCH_PROPS' => true,
					'QUANTITY' => 4
				),
				false
			); ?>
		</div>
	</div>
	<div class="container">
		<div class="about-project">
			<div class="row">
				<div class="col-xs-8 has-feedback">
					<div class="about-project-title">
						<? $APPLICATION->IncludeFile('content/front.about.html'); ?>
					</div>
				</div>

				<div class="col-xs-4">
					<div class="about-project-left">
						<? $APPLICATION->IncludeComponent(
							"newsite:ibface",
							"front.events.last",
							array(
								'DATA_TYPE' => 'ELEMENTS',
								'DATA_IBLOCK_ID' => Site::get()->scheme->events,
								'QUANTITY' => 1
							),
							false
						); ?>
					</div>
				</div>
			</div>
		</div>
		<? $APPLICATION->IncludeComponent(
			"newsite:ibface",
			"front.company.logos",
			array(
				'DATA_TYPE' => 'ELEMENTS',
				'DATA_IBLOCK_ID' => Site::get()->scheme->companies,
				'DATA_FILTER' => [
					'PROPERTY_IS_FEATURED_VALUE' => 'Y'
				]
			),
			false
		); ?>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>