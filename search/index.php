<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по вопросам");

$APPLICATION->IncludeComponent(
	"bitrix:search.page", 
	"",
	array(
		"RESTART" => "Y",
		"CHECK_DATES" => "Y",
		"arrWHERE" => "publications",
		"PAGE_RESULT_COUNT" => "100",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"COMPONENT_TEMPLATE" => "tags",
		"NO_WORD_LOGIC" => "Y",
		"USE_TITLE_RANK" => "Y",
		"DEFAULT_SORT" => "date",
		"FILTER_NAME" => "",
		"arrFILTER" => array(
			0 => "iblock_qa",
			1 => "iblock_press",
			2 => "iblock_clients",
		),
		"arrFILTER_iblock_qa" => array(
			0 => "all",
		),
		"arrFILTER_iblock_press" => array(
			0 => "5",
		),
		"arrFILTER_iblock_clients" => array(
			0 => "3",
		),
		"SHOW_WHERE" => "N",
		"SHOW_WHEN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"USE_LANGUAGE_GUESS" => "Y",
		"TAGS_SORT" => "NAME",
		"TAGS_PAGE_ELEMENTS" => "150",
		"TAGS_PERIOD" => "",
		"TAGS_URL_SEARCH" => "",
		"TAGS_INHERIT" => "Y",
		"FONT_MAX" => "50",
		"FONT_MIN" => "10",
		"COLOR_NEW" => "0",
		"COLOR_OLD" => "C8C8C8",
		"PERIOD_NEW_TAGS" => "",
		"SHOW_CHAIN" => "Y",
		"COLOR_TYPE" => "Y",
		"WIDTH" => "100%",
		"USE_SUGGEST" => "N",
		"SHOW_RATING" => "",
		"RATING_TYPE" => "",
		"PATH_TO_USER_PROFILE" => "",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Результаты поиска",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ""
	),
	false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>