<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->IncludeComponent(
	"newsite:consult.team",
	"listing",
	array(
		'QA_SECTION_ID' => arr::_($_GET, 'section'),
		'EXPERT_TOTAL_ANSWERS' => true,
		'COMPETENCE' => arr::_($_POST, 'filter')
	)
);
exit;