<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->IncludeComponent('newsite:ibface', 'faq.item.ajax',
	[
		'DATA_TYPE' => 'ELEMENTS',
		'DATA_IBLOCK_ID' => Site::get()->scheme->faq,
		'DATA_FILTER' => [
			'CODE' => arr::_($_GET, 'element', '-'),
		]
	]);
exit;