<?php
/**
 * In-place avatar generator
 *
 * @autor w
 * @date April 2016
 */

$config = [
	'title' => 'СловоДелу',
	'width' => [50, 128, 250],
	'background' => ['#39a3e1', '#017bc3', '#ffa02f', '#ff8b00', '#66b3e1', '#256a92'],
	'foreground' => ['#ffffff'],
	'font' => $_SERVER['DOCUMENT_ROOT'].'/local/templates/sd/assets/fonts/PTSansNarrowRegular/PTSansNarrow.ttf',
	'font_scale' => 0.45
];

function hex2rgb($hex){
	if (strlen($hex) == 4){
		$r = hexdec(str_repeat(substr($hex, 1, 1), 2));
		$g = hexdec(str_repeat(substr($hex, 2, 1), 2));
		$b = hexdec(str_repeat(substr($hex, 3, 1), 2));
	} else {
		$r = hexdec(substr($hex, 1, 2));
		$g = hexdec(substr($hex, 3, 2));
		$b = hexdec(substr($hex, 5, 2));
	}
	return [$r, $g, $b];
}

// two letters from input or random
$letter = mb_strtoupper(!empty($_GET['q']) && mb_strlen($_GET['q']) >= 2
	? mb_substr($_GET['q'], 0, 2)
	: mb_substr($config['title'], rand(0, mb_strlen($config['title']) - 1), 1)
	.mb_substr($config['title'], rand(0, mb_strlen($config['title']) - 1), 1));

// size in range
$width = !empty($_GET['w']) ? $_GET['w'] : $config['width'][1];
if ($width < $config['width'][0]) $width = $config['width'][0];
if ($width > $config['width'][2]) $width = $config['width'][2];
$height = $width;

// image
$image = imagecreatetruecolor($width, $height);

// background
$c = hex2rgb($config['background'][array_rand($config['background'])]);
$color_back = imagecolorallocate($image, $c[0], $c[1], $c[2]);
imagefilledrectangle($image, 0, 0, $width, $height, $color_back);

// text
$c = hex2rgb($config['foreground'][array_rand($config['foreground'])]);
$color_text = imagecolorallocate($image, $c[0], $c[1], $c[2]);
$size = $height * $config['font_scale'];
$bounds = imagettfbbox($size, 0, $config['font'], $letter);
$text_w = $bounds[2] - $bounds[0];
$text_h = $bounds[7] - $bounds[1];
$text_x = ($width - $text_w) * 0.5;
$text_y = ($height - $text_h) * 0.5;
imagettftext($image, $size, 0, $text_x, $text_y, $color_text, $config['font'], $letter);

// output
header('Content-Type: image/png');
imagepng($image);