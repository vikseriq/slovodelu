<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->IncludeComponent("newsite:consult.question.create",
	'anon',
	[
		'SECTION_CODE' => arr::_($_GET, 'section'),
		'SECTION_ID' => arr::_($_GET, 'section_id'),
		'EXPERT_ID' => arr::_($_GET, 'expert'),
		'ANON' => 'Y'
	]
);
exit; ?>
