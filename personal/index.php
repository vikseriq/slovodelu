<? define('NEED_AUTH', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел"); ?>
<div class="profile">
<? $APPLICATION->IncludeComponent('newsite:profile', '', [
	'AJAX_MODE' => 'Y'
]); ?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>