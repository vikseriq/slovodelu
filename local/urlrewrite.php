<?
/**
 * Extend BX's urlrewrite due BX bug (broke /urlrewrite.php on every(!) Visual Editor changes in front-end)
 */
$arUrlRewrite_Local = array(
	array(
		"CONDITION" => "#^/club/([^\?]*?)/.*#",
		"RULE" => "section=\$1",
		"ID" => "newsite:consult",
		"PATH" => "/club/index.php",
	),
	array(
		"CONDITION" => "#^/faq/?(.*)#",
		"RULE" => "element=\$1",
		"ID" => "newsite:ibface",
		"PATH" => "/faq/element.php",
	),
	array(
		"CONDITION" => "#^/events/?(.*)#",
		"RULE" => "element=\$1",
		"ID" => "newsite:events",
		"PATH" => "/events/index.php",
	),
	array(
		"CONDITION" => "#^/(news)/?(.*)#",
		"RULE" => "section=\$1&element=\$2",
		"ID" => "newsite:ibface",
		"PATH" => "/publications.php",
	),
	array(
		"CONDITION" => "#^/(articles)/?(.*)#",
		"RULE" => "section=\$1&element=\$2",
		"ID" => "newsite:ibface",
		"PATH" => "/publications.php",
	),
);
$bx_rewrite = $_SERVER['DOCUMENT_ROOT'].'/urlrewrite.php';
if (file_exists($bx_rewrite))
	include_once $bx_rewrite;
else
	$arUrlRewrite = [];

$arUrlRewrite = array_merge($arUrlRewrite_Local, $arUrlRewrite);
?>