<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$site = Site::get();
define('NAVCHAIN_BACK', '#BACK#');

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title><? $APPLICATION->ShowTitle() ?></title>
	<link href="<?=SITE_TEMPLATE_PATH?>/assets/css/bootstrap.css" type="text/css" rel="stylesheet">
	<link href="<?=SITE_TEMPLATE_PATH?>/assets/css/non-responsive.css" type="text/css" rel="stylesheet">
	<link href="<?=SITE_TEMPLATE_PATH?>/assets/css/main.css" type="text/css" rel="stylesheet">
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/jquery-1.11.3.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/jquery.flexisel.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/bootstrap.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/main.js"></script>
	<? $APPLICATION->ShowHead() ?>
	<!--[if lte IE 8]>
	<link href="<?=SITE_TEMPLATE_PATH?>/assets/css/ie8-compability.css" type="text/css" rel="stylesheet">
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/ie8-compability.js"></script>
	<![endif]-->
</head>

<body><? $APPLICATION->ShowPanel(); ?>
<div class="header">
	<nav class="navbar header-navbar">
		<div class="container">
			<div class="row">
				<div class="navbar-header">
					<a href="/" class="navbar-brand"></a>
				</div>
			</div>
		</div>
	</nav>
	<nav class="navbar sub-navbar">
		<div class="container">
			<div class="row">
				<div class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
					<div class="row">
						<? $APPLICATION->IncludeComponent('bitrix:menu', 'primary', array(
							'ROOT_MENU_TYPE' => 'primary',
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "N"
						)); ?>
						<div class="navbar-search">
							<form class="navbar-form search-form" role="search" method="get" action="/search/">
								<div class="input-group">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-search pull-right">
										<span class="glyphicon glyphicon-search"></span>
									</button>
                                </span>
									<input type="text" name="q" class="form-control main-style-text"
										   value="<?=htmlentities(arr::_($_GET, 'q'))?>" placeholder="Поиск">
								</div>
							</form>
							<button type="button" class="btn btn-search control">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>
</div>
<? $APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default"); ?>
<? if (!defined('UI_NOWRAP')): ?><div class="container wrap"><? endif ?>