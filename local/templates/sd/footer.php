<? defined("B_PROLOG_INCLUDED") || die('hard'); ?>
<? if (!defined('UI_NOWRAP')): ?></div><? endif ?>
<div class="panel-footer">
	<div class="container clearfix">
		<ul class="col-2">
			<? $APPLICATION->IncludeComponent(
				"bitrix:menu",
				"footer",
				array(
					"ROOT_MENU_TYPE" => "footer_left",
					"CACHE_TYPE" => "A",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "N"
				),
				false
			); ?>
		</ul>
		<ul class="col-1">
			<? $APPLICATION->IncludeComponent(
				"bitrix:menu",
				"footer",
				array(
					"ROOT_MENU_TYPE" => "footer_center",
					"CACHE_TYPE" => "A",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "N"
				),
				false
			); ?>
		</ul>
		<ul class="col-3">
			<? $APPLICATION->IncludeComponent(
				"bitrix:menu",
				"footer",
				array(
					"ROOT_MENU_TYPE" => "footer_right",
					"CACHE_TYPE" => "A",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_USE_GROUPS" => "N"
				),
				false
			); ?>
		</ul>
		<div class="col-4 pull-right">
			<img src="/media/zaglushka.jpg">
		</div>
	</div>
</div>
<div class="sub-footer">
	<div class="container">
		<div class="row">
			<ul class="col-xs-12">
				<li class="col-1 pull-left">
					<div class="footer-logo">
						<span></span>
					</div>
				</li>
				<li class="col-2 pull-left basic-text">&copy; 2011–<?=date('Y')?> ЗАО «Белгазпромбанк»</li>
				<li class="col-3 pull-left basic-text">
					<a href="http://newsite.by/">Разработка сайта — Новый сайт</a>
					<span class="hidden">vikseriq</span>
				</li>
			</ul>
		</div>
	</div>
</div>
<div id="modal-base" role="dialog" tabindex="-1" class="clearfix modal fade modal-remote container">
	<div class="modal-body question-form container"></div>
	<button type="button" class="btn btn-close" data-dismiss="modal"></button>
</div>
<? include __DIR__.'/content/metrics.php' ?>
</body>
</html>