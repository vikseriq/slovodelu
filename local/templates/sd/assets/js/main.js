/**
 * SlovoDelu '16
 *
 * @author NewSite team
 */

/**
 * Search field in navbar
 */
var searchBar = {
	selfClass: '.navbar-search',
	searchForm: '.navbar-search .navbar-form.search-form',
	buttonControl: {
		selfClass: '.btn-search.control',
		accessory: {
			selfClass: '.btn-search.control span.glyphicon',
			open: 'glyphicon-search',
			close: 'glyphicon-remove'
		}
	},
	visible: false,
	offset: 0,

	init: function(){
		$(this.buttonControl.selfClass).click(function(){
			searchBar.toggle();
		});
		this.offset = $('.sub-navbar .navbar-collapse').width() - 18;
    // autotoggle if filled
    if ($(this.searchForm).find('input').val())
      this.toggle();
	},

	toggle: function(){
		this[this.visible ? 'hide' : 'show']();
		$(this.selfClass).toggleClass('active');
		$(this.buttonControl.accessory.selfClass)
			.toggleClass(searchBar.buttonControl.accessory.open)
			.toggleClass(searchBar.buttonControl.accessory.close);
	},

	show: function(){
		if (this.visible)
			return;
		$(this.searchForm).animate({
			width: "+=" + this.offset
		}, 300);
		this.visible = true;
    $(this.searchForm).find('input').focus();
	},

	hide: function(){
		if (!this.visible)
			return;
		$(this.searchForm).animate({
			width: "-=" + this.offset
		}, 300);
		this.visible = false;
	}
};

var Siteframe = {
	scrollTo: function(target, onComplete){
		$('html,body').animate({
			scrollTop: target.offset().top - $('.sub-navbar').outerHeight() - 10
		}, 300, onComplete);
	},
	inlinePager: {
		init: function(){
			$('.js-ipage-next').click(function(){
				var page = $(this).attr('data-page') || 1;
				var next = Siteframe.inlinePager.next({
					pager: $(this).data('pager'),
					page: ++page
				});
				if (next)
					$(this).attr('data-page', page);
				else
					$(this).hide();
			});
		},
		next: function(options){
			var pager = $('.js-ipage[data-pager=' + options.pager + ']');
			if (!pager)
				return false;
			var elements = pager.find('div[data-page=' + options.page + ']');
			if (!elements)
				return false;
			elements.removeClass('hidden');
			return pager.find('div[data-page=' + (options.page + 1) + ']').length ? true : false;
		}
	}
};

$(document).ready(function(){
	// stick top menu
	var heightTopMenu = $('.navbar.header-navbar').height();
	$(window).scroll(function(){
		if ($(this).scrollTop() >= heightTopMenu){
			$('.header').addClass('fixed');
		} else
			$('.header').removeClass('fixed')
	});

	// search panel
	searchBar.init();

	// fit frontpage carousel items width
	$('.carousel-indicators').each(function(){
		var childs = $(this).find(".carousel-indicators-inner");
		var width = Math.floor(100 / childs.length) + "%";
		childs.each(function(){
			$(this).css('width', width);
		})
	});

	// frontpage company logos slider
	$(".logo-slider.carousel").flexisel();

	// descriptive popup handler
	var popup_timeout = 300;
	$('[data-toggle = "popup"]')
		.mouseenter(function(){
			var attribute = this.getAttribute('data-target');

			setTimeout(function(){
				$(attribute).addClass('show');
			}, popup_timeout)
		})
		.mouseleave(function(){
			var attribute = this.getAttribute('data-target');

			setTimeout(function(){
				if (!$(attribute).is(':hover')){
					setTimeout(function(){
						$(attribute).removeClass('show');
					}, popup_timeout);
				} else {
					$(attribute).mouseleave(function(){
						setTimeout(function(){
							$(attribute).removeClass('show');
						}, popup_timeout);
					});
				}
			}, popup_timeout);
		});

	// remote modals
	$('.modal-remote').on('show.bs.modal', function(e){
		var src = $(this).attr('data-src');
		if (!src){
			var caller = $(e.relatedTarget);
			src = caller.attr('data-src');
		}
		var modal = $(this);
    // adjust view position
    modal.css('top', 'calc(20vh + ' + $(window).scrollTop() + 'px)')

    // load content
		$.get(src, function(data){
			modal.find('.modal-body').html(data);
			modal.find('[data-toggle=collapse]').collapse();
			modal.find('[data-toggle=dropdown]').dropdown();
      modal.focus();
		});
	});
});