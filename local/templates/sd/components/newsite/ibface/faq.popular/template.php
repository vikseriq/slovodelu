<?
$i = 0;
$iMax = 5;
$n = count($arResult['DATA']);
if (!$n) return;
$base = reset($arResult['DATA'])['LIST_PAGE_URL'];
?>
<div class="popular-question">
	<div class="box-title bottom-border">
		<h2>Популярные вопросы</h2>
	</div>
	<ol class="question-list bottom-border">
		<? foreach ($arResult['DATA'] as $item):
			if ($i++ >= $iMax) break;
			?>
			<li class="main-style-text"><a href="<?=$item['DETAIL_PAGE_URL']?>" data-toggle="modal"
										   data-target="#modal-base"
										   data-src="/ajax/faq.php?element=<?=$item['CODE']?>"
					><?=$item['NAME']?></a></li>
		<? endforeach ?>
	</ol>
	<? if ($n > $iMax): ?>
		<div class="box-footer text-center bottom-border">
			<h4><a href="<?=$base?>">Все популярные вопросы (<?=($n - $iMax)?>)</a></h4>
		</div>
	<? endif ?>
</div>