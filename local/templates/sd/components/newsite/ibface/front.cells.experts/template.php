<?
$sections = $arResult['DATA'];
$n = count($sections);
$i = 0;
$iMax = 8;
$base = reset($arResult['DATA'])['LIST_PAGE_URL'];
// extend data: collect company count
$companies = Site::get()->getCompanies(['ACTIVE' => 'Y']);
foreach($companies as $item){
	foreach($item['QA_SECTIONS'] as $section_id){
		if (isset($sections[$section_id])){
			$sections[$section_id]['ELEMENT_CNT'] = arr::_($sections[$section_id], 'ELEMENT_CNT', 0) + 1;
		}
	}
}
?>
<div class="box-title">
	<h2>Бизнес-клуб</h2>
</div>
<div class="inner-box-background">
	<div class="inner-box">
		<? foreach ($sections as $item):
			if ($i++ >= $iMax)
				break;
			?>
			<a href="<?=$item['SECTION_PAGE_URL']?>" class="col-xs-3">
				<p class="main-style-text"><?=$item['NAME']?></p>
				<span class="basic-text"><? if ($item['ELEMENT_CNT']): ?><span class="icon-chat"></span><?=$item['ELEMENT_CNT']?><? endif ?></span>
			</a>
		<? endforeach ?>
	</div>
</div>
<div class="box-footer text-center bottom-border"><h4><a href="<?=$base?>">Все партнеры (<?=count($companies)?>)</a></h4></div>