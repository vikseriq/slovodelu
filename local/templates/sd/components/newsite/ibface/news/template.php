<?
$section = reset($arResult['DATA']);
if (!$section)
	return Site::notFound();

Site::setSefParams($section, true);
$APPLICATION->AddChainItem($section['NAME'], $section['SECTION_PAGE_URL']);

?>
<div class="news margin-bottom">
	<h1><?=$section['NAME']?></h1>
	<? $APPLICATION->IncludeComponent('newsite:ibface', 'news.list', [
		'DATA_TYPE' => 'ELEMENTS',
		'DATA_IBLOCK_ID' => $section['IBLOCK_ID'],
		'DATA_FILTER' => [
			'SECTION_CODE' => $section['CODE']
		],
		'DATA_FETCH_PROPS' => true
	]) ?>
</div>