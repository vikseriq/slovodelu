<?php
$items = $arResult['DATA'];
// show subscription form after $iSubscribeRow items
$iSubscribeRow = 4;
if (!count($items)): ?>
	<div class="text-muted margin-bottom">
		В настоящий момент материалы отсутствуют
	</div>
<? else: ?>
	<ul class="news-card">
		<? $i = 0;
		foreach ($items as $item):
			$i++;
			$image = $item['DETAIL_PICTURE'] ? CFile::GetPath($item['DETAIL_PICTURE']) : '';
			?>
			<li class="col-xs-3 col-xs-6">
				<a href="<?=$item['DETAIL_PAGE_URL']?>" class="news-card-content <?=$item['TYPE']['VALUE_XML_ID']?>">
					<span class="news-card-image"><img src="<?=$image?>"/></span>

					<div class="news-card-text">
						<time class="time"
							  title="<?=Site::date($item['DATE_CREATE'])?>"><?=Site::date($item['ACTIVE_FROM'], 'DATE_RELATIVE')?></time>

						<p class="main-style-text"><?=$item['NAME']?></p>
					</div>
				</a>
			</li>
			<? if ($i == $iSubscribeRow):
			echo '</ul>';
			$APPLICATION->IncludeComponent(
				"bitrix:sender.subscribe",
				"",
				Array(
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"COMPONENT_TEMPLATE" => ".default",
					"CONFIRMATION" => "N",
					"SET_TITLE" => "N",
					"SHOW_HIDDEN" => "N",
					"USE_PERSONALIZATION" => "Y"
				)
			);
			echo '<ul class="news-card">';
		endif;
		endforeach ?>
	</ul>
<? endif ?>