<div class="left-panel-title text-center"><h4>Последние вопросы</h4></div>
<ul class="last-questions">
	<? foreach($arResult['DATA'] as $item): ?>
	<li><a href="<?=$item['DETAIL_PAGE_URL']?>" class="basic-text"><?=$item['NAME']?></a></li>
	<? endforeach ?>
</ul>
<div class="left-panel-btn">
	<button class="btn btn-default btn-block">Задать вопрос</button>
</div>