<?
$item = reset($arResult['DATA']);
if (!$item)
	return Site::notFound();

Site::setSefParams($item);
// Fetching section data for navchain
$ob = CIBlockSection::GetList([], ['IBLOCK_ID' => $item['IBLOCK_ID'], 'ID' => $item['IBLOCK_SECTION_ID']]);
$section = $ob->GetNext();
if ($section){
	$APPLICATION->AddChainItem($section['NAME'], $section['SECTION_PAGE_URL']);
	$APPLICATION->AddChainItem(NAVCHAIN_BACK.'К списку публикаций', $section['SECTION_PAGE_URL']);
}
?>
<div class="news-page">
	<h1><?=$item['NAME']?></h1>

	<div class="news-stat">
		<time class="secondary-text"
			  title="<?=Site::date($item['DATE_CREATE'], 'DETAIL')?>"><?=Site::date($item['DATE_CREATE'], 'FULL_RELATIVE')?></time>
	</div>
	<div>
		<?=$item['DETAIL_TEXT']?>
	</div>
</div>
