<?
$n = count($arResult['DATA']);
$i = 0;
$iMax = 6;
$base = reset($arResult['DATA'])['LIST_PAGE_URL'];
?>
<div class="box-title">
	<h2>Вопросы и ответы по темам</h2>
</div>
<div class="inner-box-background">
	<div class="inner-box">
		<? foreach($arResult['DATA'] as $item):
			if ($i++ >= $iMax)
				break;
			?>
		<a href="<?=$item['SECTION_PAGE_URL']?>" class="col-xs-4 col-xs-6">
			<p class="main-style-text"><?=$item['NAME']?></p>
			<span class="basic-text"><span class="icon-chat"></span><?=$item['ELEMENT_CNT']?></span>
		</a>
		<? endforeach ?>
	</div>
</div>
<div class="box-footer text-center bottom-border"><h4><a href="<?=$base?>">Другие темы (<?=$n - $iMax?>)</a></h4></div>