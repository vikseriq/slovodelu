<? $n = count($arResult['DATA']); if (!$n) return;
$faceimg = '/media/expert%d.png'; $faceimg_n = 3;
?>
<div class="row">
	<div class="col-xs-5">
		<span class="img-circle" style="background-image: url('<? printf($faceimg, rand(0, $faceimg_n)); ?>');"></span>
	</div>
	<div class="col-xs-7 none-padding">
		<h2><?=$n.' '.txt::plural($n, ['эксперт', 'эксперта', 'экспертов'])?></h2>
		<p class="main-style-text">
			<?=txt::plural($n, ['готов', 'готовы', 'готовы'])?> ответить на вопросы про бизнес
		</p>
		<button class="btn btn-default btn-block">Задать вопрос</button>
	</div>
</div>