<?
if (!$arResult['DATA'])
	return;
?>
<div class="box-title">
	<h2 class="pull-left margin">Новости и статьи</h2>

	<div class="pull-right links">
		<h4 class="pull-left"><a href="/news/">Все новости</a></h4>
		<h4 class="pull-left"><a href="/articles/">Все статьи</a></h4>
	</div>

</div>
<ul class="news-card">
	<? foreach ($arResult['DATA'] as $item):
		$image = $item['DETAIL_PICTURE'] ? CFile::GetPath($item['DETAIL_PICTURE']) : '';
		?>
		<li class="col-xs-3 col-xs-6">
			<a href="<?=$item['DETAIL_PAGE_URL']?>" class="news-card-content <?=$item['TYPE']['VALUE_XML_ID']?>">
				<span class="news-card-image"><img src="<?=$image?>"/></span>

				<div class="news-card-text">
					<time class="time" title="<?=Site::date($item['DATE_CREATE'])?>"><?=Site::date($item['ACTIVE_FROM'], 'DATE_RELATIVE')?></time>

					<p class="main-style-text"><?=$item['NAME']?></p>
				</div>
			</a>
		</li>
	<? endforeach ?>
</ul>