<?
$event = $arResult['DATA'] ? reset($arResult['DATA']) : [];
$logo = $event && $event['PREVIEW_PICTURE']
	? CFile::GetPath($event['PREVIEW_PICTURE'])
	: Site::vendeface('СД');
?>
<div class="row">
	<div class="col-xs-5">
		<span class="img-circle" style="background-image: url('<?=$logo?>');"></span>
	</div>
	<div class="col-xs-7 none-padding">
		<h2>Мероприятия</h2>
		<? if ($event){ ?>
			<p class="main-style-text"><?=$event['NAME']?></p>
			<a href="<?=$event['DETAIL_PAGE_URL']?>" class="btn btn-default btn-block">Подробнее</a>
		<? } else { ?>
			<p class="main-style-text">В ближайшее время нет мероприятий</p>
		<? } ?>
	</div>
</div>