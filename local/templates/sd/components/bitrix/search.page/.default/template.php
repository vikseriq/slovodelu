<?php
$site = Site::get();
$searchResults = $arResult['SEARCH'];
$results = [];
$cases = [
	'COMPANIES' => [
		'TITLE' => 'Компании'
	],
	'QUESTIONS' => [
		'TITLE' => 'Вопросы к экспертам'
	],
	'ANSWERS' => [
		'TITLE' => 'Ответы на вопросы'
	],
	'PUBLICATIONS' => [
		'TITLE' => 'Публикации'
	],
];

if ($searchResults){
	$params = [];
	foreach ($searchResults as $sr){
		$params[] = arr::mapify(['IBLOCK_ID@PARAM2', 'ID@ITEM_ID', 'MATCH@BODY_FORMATED'], $sr);
	}
	$results = $site->aggregateSearch($params);
}

if (!$results): ?>
	<div class="my_questions">
		<div class="box-title bottom-border">
			<h2>Поиск</h2>
		</div>
		<ul class="my_questions-list">
			<li class="row question-line">
				Нет результатов по вашему поисковому запросу
			</li>
		</ul>
	</div>
<? else: ?>
	<? foreach ($cases as $case_key => $case_var):
		$segment = arr::_($results, $case_key);
		if (!$segment)
			continue;
		?>
		<div class="my_questions">
			<div class="box-title bottom-border">
				<h2><?=$case_var['TITLE']?></h2>
			</div>
			<ul class="my_questions-list">
				<? foreach ($segment as $item): ?>
					<li class="row question-line">
						<div class="col-xs-4 question-column">
							<a href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a>
						</div>
						<div class="col-xs-8">
							<?=$item['MATCH']['MATCH']?>
						</div>
					</li>
				<? endforeach ?>
			</ul>
		</div>
	<? endforeach ?>
<? endif ?>