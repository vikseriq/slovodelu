<? defined('B_PROLOG_INCLUDED') || die('hard'); ?>
<? foreach ($arResult as $menu):
	if (arr::_($menu['PARAMS'], 'IS_QA') == 'Y'):
		?>
		<li><h3><a href="<?=$menu['LINK']?>"><?=$menu['TEXT']?></a></h3></li>
		<? $themes = Site::get()->getQASections();
		if ($themes):
			$maxLines = arr::_($arParams, 'MAX_LINES', 6);
			if (count($themes) > $maxLines){
				$themes = array_slice($themes, 0, $maxLines);
				array_push($themes, [
					'SECTION_PAGE_URL' => $menu['LINK'],
					'NAME' => arr::_($arParams, 'MORE_TEXT', 'Все категории')
				]);
			}
			foreach ($themes as $t): ?>
				<li class="basic-text"><a href="<?=$t['SECTION_PAGE_URL']?>"><?=$t['NAME']?></a></li>
			<? endforeach ?>
		<? endif;
		else: ?>
			<? if (arr::_($menu['PARAMS'], 'CHILD')): ?>
				<li class="basic-text"><a href="<?=$menu['LINK']?>"><?=$menu['TEXT']?></a></li>
			<? else: ?>
				<li><h3><a href="<?=$menu['LINK']?>"><?=$menu['TEXT']?></a></h3></li>
			<? endif ?>
		<? endif ?>
<? endforeach ?>
