<?php
$arTemplateParameters = array(
	'MAX_LINES' => array(
		'NAME' => 'Максимальное количество пунктов',
		'TYPE' => 'TEXT',
		'DEFAULT' => '6'
	),
	'MORE_TEXT' => array(
		'NAME' => 'Текст для перехода ко всем пунктам',
		'TYPE' => 'TEXT',
		'DEFAULT' => 'Все категории'
	)
);