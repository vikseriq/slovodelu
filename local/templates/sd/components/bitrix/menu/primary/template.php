<? defined('B_PROLOG_INCLUDED') || die('hard'); ?>
<ul class="nav navbar-nav">
	<? foreach ($arResult as $menu):
		$class_active = $menu['SELECTED'] ? ' active' : '';
		if (arr::_($menu['PARAMS'], 'IS_QA') == 'Y'):
			?>
			<li class="nav-item<?=$class_active?> dropdown">
				<a href="<?=$menu['LINK']?>" class="main-style-text dropdown-toggle" data-toggle="dropdown"
				   role="button"
				   aria-haspopup="true"
				   aria-expanded="false"><?=$menu['TEXT']?>
					<span
						class="show-on dropdown-accessory glyphicon glyphicon-chevron-down"></span>
					<span class="show-off dropdown-accessory glyphicon glyphicon-chevron-up"></span></a>
				<? $themes = Site::get()->getQASections();
				if ($themes):
					$maxLines = arr::_($arParams, 'MAX_LINES', 6);
					if (count($themes) > $maxLines){
						$themes = array_slice($themes, 0, $maxLines);
						array_push($themes, [
							'SECTION_PAGE_URL' => $menu['LINK'],
							'SHORT_NAME' => arr::_($arParams, 'MORE_TEXT', 'Все категории')
						]);
					}
					?>
					<ul class="dropdown-menu">
						<? foreach ($themes as $t): ?>
							<li><a href="<?=$t['SECTION_PAGE_URL']?>"><?=$t['SHORT_NAME']?></a></li>
						<? endforeach ?>
					</ul>
				<? endif ?>
			</li>
		<? else: ?>
			<li class="nav-item<?=$class_active?>"><a class="main-style-text" href="<?=$menu['LINK']?>"><?=$menu['TEXT']?></a></li>
		<? endif ?>
	<? endforeach ?>
</ul>