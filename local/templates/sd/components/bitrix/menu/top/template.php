<? defined('B_PROLOG_INCLUDED') || die('hard');
$user = Site::get()->getCurrentUser();
?>
<ul class="nav navbar-nav navbar-right">
	<? if (is_array($arResult)) foreach ($arResult as $menu): ?>
		<li class="basic-text"><a href="<?=$menu['LINK']?>"><?=$menu['TEXT']?></a></li>
	<? endforeach ?>
	<? if (!$user['ID']): ?>
		<li class="basic-text">
			<a href="<?=Site::get()->scheme->personal_area?>" data-toggle="modal"
			   data-target="#modal-base"
			   data-src="/ajax/profile.php?AJAX_CALL=Y">
				<span class="glyphicon glyphicon-user"></span>Войти</a>
		</li>
	<? else: ?>
		<li class="basic-text login">
			<a href="#" class="dropdown-toggle no-underline" data-toggle="dropdown" role="button"
			   aria-haspopup="true" aria-expanded="false">
				<span class="img-circle" style="background-image: url('<?=$user['AVATAR']?>');"></span>
				<span class="user-name"><?=$user['FULL_NAME']?></span>
				<span class="show-on login-accssory glyphicon glyphicon-chevron-down"></span>
				<span class="show-off login-accssory glyphicon glyphicon-chevron-up"></span>
			</a>
			<ul class="dropdown-menu dropdown-triangle">
				<li><a href="/personal/">Вопросы</a></li>
				<li><a href="/personal/settings/">Настройки профиля</a></li>
				<li><a href="/personal/?logout=yes">Выход</a></li>
			</ul>
		</li>
	<? endif ?>
</ul>
