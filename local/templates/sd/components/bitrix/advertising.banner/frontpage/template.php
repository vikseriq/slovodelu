<? defined("B_PROLOG_INCLUDED") || die('hard');

$data = arr::_($arResult, 'BANNERS_PROPERTIES');
if (!$data) return;

$banners = Site::extractBanners($arResult);
?>
<div id="frontCarousel" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner" role="listbox">
		<? foreach ($banners as $i => $item): ?>
			<div class="item <?=($i == 0) ? 'active' : ''?>">
				<img src="<?=$item['IMAGE']?>"/>

				<div class="container">
					<a href="<?=$item['LINK']?>" class="carousel-caption">
						<h1><?=$data[$i]['IMAGE_ALT']?></h1>
					</a>
				</div>
			</div>
		<? endforeach ?>
	</div>
	<a class="left carousel-control" href="#frontCarousel" role="button" data-slide="prev">
		<span aria-hidden="true"></span>
	</a>
	<a class="right carousel-control" href="#frontCarousel" role="button" data-slide="next">
		<span aria-hidden="true"></span>
	</a>

	<div class="carousel-indicators">
		<? foreach ($data as $i => $item): ?>
			<div class="carousel-indicators-inner <?=($i == 0) ? 'active' : ''?>"
				 data-target="#frontCarousel" data-slide-to="<?=$i?>">
				<div class="indicator"></div>
				<div class="carousel-card">
					<span class="basic-text"><?=$item['NAME']?></span>
				</div>
			</div>
		<? endforeach ?>
	</div>
</div>
