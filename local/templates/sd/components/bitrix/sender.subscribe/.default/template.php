<?
$noFeedback = empty($arResult['MESSAGE']);

// hide for already subscribed users
if ($noFeedback && !empty($_SESSION['SENDER_SUBSCRIBE_LIST']) && $_SESSION['SENDER_SUBSCRIBE_LIST']['SUBSCRIPTION']['ID'])
	return;

$showForm = $noFeedback || $arResult['MESSAGE']['TYPE'] == 'ERROR';
$message = $noFeedback ? '' : $arResult['MESSAGE']['TEXT'];
?>
<div class="bx-subscribe" id="sender-subscribe">
	<div class="banner-forth">
		<div class="content">
			<? if ($showForm): ?>
				<p class="psevdo-h2"><?=txt::ab($message, 'Подпишитесь на рассылку новстей о бизнесе!')?></p>
				<form role="form" method="post" action="<?=$arResult["FORM_ACTION"]?>">
					<?=bitrix_sessid_post()?>
					<input type="hidden" name="sender_subscription" value="add">
					<? foreach ($arResult["RUBRICS"] as $itemID => $itemValue): ?>
						<input type="hidden" name="SENDER_SUBSCRIBE_RUB_ID[]" value="<?=$itemValue["ID"]?>"/>
					<? endforeach ?>

					<div class="form-group basic-text input-group">
						<input type="email" class="form-control" name="SENDER_SUBSCRIBE_EMAIL"
							   value="<?=$arResult["EMAIL"]?>" placeholder="Введите Ваш E-mail">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary">Подписаться</button>
                    </span>
					</div>
				</form>
			<? else: ?>
				<p class="psevdo-h2"><?=$message?></p>
			<? endif ?>
		</div>
	</div>
</div>