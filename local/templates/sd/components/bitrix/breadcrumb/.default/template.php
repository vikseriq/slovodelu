<? if (!defined('B_PROLOG_INCLUDED')) die('hard');
if (!$arResult || count($arResult) == 1)
	return '';

$html = '<div class="container"><div class="row"><ol class="breadcrumb">';

$html_li = '';
foreach ($arResult as $item){
	if (substr($item['TITLE'], 0, strlen(NAVCHAIN_BACK)) == NAVCHAIN_BACK){
		$html .= '<li class="active"><a class="secondary-text" href="'.$item['LINK'].'">'
			.str_replace(NAVCHAIN_BACK, '', $item['TITLE']).'</a></li>';
		continue;
	}
	$html_li .= '<li><a class="secondary-text" href="'.txt::ab($item['LINK'], '#').'">'.$item['TITLE'].'</a></li>';
}
$html .= $html_li.'</ol></div></div>';

return $html;