<?php

/**
 * Token-based cross app authenticator
 * For Bitrix and external apps
 *
 * @author w@newsite.by
 * @date Dec 1, 2015
 */
class NS_CrossAuth {

	/**
	 * @var string Common GET param name where we looking for token
	 */
	static $param = 'eid';
	/**
	 * @var int Auth sequence time to live in seconds.
	 */
	static $ttl = 86400;

	/**
	 * Generating token string by login
	 * @param string $login User's login or ID or empty for current user
	 * @return bool|string Token or false
	 */
	static function getToken($login = null){
		global $USER;
		if (!$login && !($login = $USER->GetLogin()))
			return false;

		$ob = CUser::GetByID($login);
		if (!$ob)
			$ob = CUser::GetByLogin($login);
		if (!$ob)
			return false;

		$user_data = $ob->Fetch();
		if (!$user_data)
			return false;

		return $user_data['ID'].':'.self::makeHash($user_data);
	}

	/**
	 * Build uri fragment with crossAuth token for current user
	 * @param $login string Login or ID of user to auth as
	 * @return string URI fragment with hash
	 */
	static function getUriFragment($login){
		$token = self::getToken($login);
		return $token ? '&'.self::$param.'='.$token : "";
	}

	/**
	 * Generate hash based on user token
	 * @param $user_data array User-related params
	 * @param $time_offset int time-based discretization offset
	 * @return string Hash of token
	 */
	static function makeHash($user_data, $time_offset = 0){
		return md5($user_data['PASSWORD'].$_SERVER['DOCUMENT_ROOT'].date('Ymd', time() + $time_offset * self::$ttl));
	}

	/**
	 * Handling auth process: check token and auth if correct
	 * @param $token string Token from request
	 */
	static function auth($token){
		$parts = explode(':', $token);
		if (count($parts) != 2)
			return;
		$user_data = CUser::GetByID((int)$parts[0])->Fetch();
		if (!$user_data)
			return;
		$hash = self::makeHash($user_data);
		if ($hash != $parts[1]){
			// also check prev tokens sequence
			$hash = self::makeHash($user_data, -1);
			if ($hash != $parts[1])
				return;
		}

		global $USER, $APPLICATION;
		$USER->Authorize($user_data['ID']);
		$url = $APPLICATION->GetCurPageParam("", array(self::$param));
		LocalRedirect($url);
		exit;
	}

	/**
	 * Handling url and Auth&Redirect if correct token passed
	 */
	static function HandleBitrix(){
		if (isset($_GET[self::$param])){
			self::auth($_GET[self::$param]);
		}
	}

}

// place handler
AddEventHandler("main", "OnBeforeProlog", array("NS_CrossAuth", "HandleBitrix"));