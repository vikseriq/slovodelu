<?php
/**
 *      --=== SlovoDelu ===--
 * Q&A site for entrepreneurship
 *
 * @author w@newsite.by
 * @date March 2016
 * @license MIT
 */

/**
 * Common business-logic and data management singleton
 * Class Site
 */
error_reporting(E_ALL && ~E_STRICT);
ini_set('display_errors', 1);

include_once __DIR__.'/prop_schedule.php';

class Site {

	/**
	 * @var Site shared instance
	 */
	private static $_instance = null;

	/**
	 * @return Site returns shared instance
	 */
	public static function get(){
		if (!self::$_instance)
			self::$_instance = new Site();
		return self::$_instance;
	}

	static $sort_asc = ["SORT" => "ASC"];
	static $sort_desc = ["SORT" => "DESC"];

	public static $isAjax = false;

	/**
	 * @var SiteScheme
	 */
	var $scheme;
	var $user = [];
	var $last_error = null;

	var $allowAnon = true;
	var $moderateQuestions = false;

	public function __construct(){
		CModule::IncludeModule('iblock') || die('No BX::iblock module found');
		$this->scheme = new SiteScheme();

		Site::$isAjax = arr::_($_POST, 'AJAX_CALL') == 'Y' || arr::_($_REQUEST, 'bxajaxid');

		$this->user = $this->getCurrentUser();

		$this->bindHooks();

		if ($this->user['ID'] && Site::$isAjax){
			$this->handleAuth();
		}
	}

	public function error($message){
		$this->last_error = $message;
		return false;
	}

	public static function bindHooks(){
		AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", [__CLASS__, "onIBlockUpdate"]);
		AddEventHandler("iblock", "OnAfterIBlockElementUpdate", [__CLASS__, "onIBlockUpdate"]);
	}

	public static function onIBlockUpdate($element){
		$onAfter = arr::_($element, 'RESULT') == 1;
		if (!$onAfter){
			if ($element['IBLOCK_ID'] == SiteScheme::ib_questions){
				Site::get()->onQuestionUpdate($element);
			}
		}
	}

	public function handleAuth(){
		global $APPLICATION;
		if (arr::_($_POST, 'AUTH_FORM') == 'Y' && arr::_($_POST, 'TYPE') == 'AUTH'){
			$APPLICATION->RestartBuffer();
			if ($back = arr::_($_POST, 'BACKURL'))
				$command = 'href = "'.$back.'"';
			else
				$command = 'reload()';
			echo '<script>window.parent.location.'.$command.';</script>';
			exit;
		}
	}

	public function crossAuthLink($base_url, $user_id){
		$token = (class_exists('NS_CrossAuth')) ? NS_CrossAuth::getUriFragment($user_id) : '';
		return $base_url.'?'.$token;
	}

	public function getAccessFilter($element_group, $element_id = null){
		$filter = [];
		$user = $this->getCurrentUser();
		if ($element_group == 'question_list' || $element_group == 'question'){
			// everybody can view public questions
			// author + his questions
			// expert + questions with company-wide access
			if (!$user['ID'])
				$filter['ACTIVE'] = 'Y';
			else {
				if (!$user['EXPERT']){
					$filter[] = [
						'LOGIC' => 'OR',
						['ACTIVE' => 'Y'],
						['PROPERTY_AUTHOR_USER' => $user['ID']]
					];
				} else {
					if (!in_array($element_id, $user['COMPANY']['QA_SECTIONS']))
						$filter['ACTIVE'] = 'Y';
				}
			}
		}

		return $filter;
	}

	public function canPutQuestion(){
		$user = $this->getCurrentUser();
		return $user['ID'] && !$user['EXPERT'];
	}

	public function getQASections(){
		static $cached = [];
		if ($cached)
			return $cached;

		$ob = CIBlockSection::GetList(self::$sort_asc, ['IBLOCK_ID' => $this->scheme->questions], false, ['UF_*']);
		$ar = [];
		while ($e = $ob->GetNext()){
			$e['SHORT_NAME'] = txt::ab($e['UF_SHORT_NAME'], $e['NAME']);
			$e['LINK'] = $e['SECTION_PAGE_URL'];
			$ar[$e['CODE']] = $e;
		}
		$cached = $ar;
		return $ar;
	}

	public function getQASection($code, $id = null){
		$sections = $this->getQASections();
		foreach ($sections as $s){
			if ($code && $s['CODE'] == $code)
				return $s;
			else if ($id && $s['ID'] == $id)
				return $s;
		}
		return false;
	}

	public function getQuestions($_filter, $single = false){
		static $map = [
			'ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID',
			'NAME', 'DATE_CREATE', 'DETAIL_TEXT',
			'DETAIL_PAGE_URL', 'LIST_PAGE_URL',
			'AUTHOR_USER@AUTHOR_USER:VALUE',
			'DISABLE_REPLY@DISABLE_REPLY:VALUE',
			'AUTHOR_PRIVACY@AUTHOR_PRIVACY:VALUE',
			'DIRECT_EXPERT@DIRECT_EXPERT:VALUE',
		];
		if ($this->allowAnon){
			$map[] = 'ANON_NAME@ANON_NAME:VALUE';
			$map[] = 'ANON_EMAIL@ANON_EMAIL:VALUE';
		}
		$filter = array_merge(['IBLOCK_ID' => $this->scheme->questions], $_filter);
		$ob = CIBlockElement::GetList(self::$sort_asc, $filter);

		$data = [];
		while ($ar = $ob->GetNextElement()){
			$arItem = array_merge($ar->GetFields(), $ar->GetProperties());
			$data[$arItem['ID']] = arr::mapify($map, $arItem);
			if ($single)
				return $data[$arItem['ID']];
		}

		return $data;
	}

	public function getAnswers($question_id){
		static $map = [
			'ID', 'IBLOCK_ID', 'DATE_CREATE', 'DETAIL_TEXT',
			'TIMESTAMP_CREATE',
			'QUESTION_ID@QUESTION:VALUE',
			'AUTHOR_USER@AUTHOR_USER:VALUE',
			'AUTHOR_EXPERT@AUTHOR_EXPERT:VALUE',
			'PARENT_ID@PARENT:VALUE'
		];

		$filter = [
			'IBLOCK_ID' => $this->scheme->answers,
			'PROPERTY_QUESTION' => $question_id
		];
		$ob = CIBlockElement::GetList(self::$sort_asc, $filter);

		$data = [];
		while ($ar = $ob->GetNextElement()){
			$arItem = array_merge($ar->GetFields(), $ar->GetProperties());
			$arItem['TIMESTAMP_CREATE'] = strtotime($arItem['DATE_CREATE']);
			$data[$arItem['ID']] = arr::mapify($map, $arItem);
		}

		return $data;
	}

	public function putQuestion($author_id, $data){
		$now = time();
		$question = [
			'IBLOCK_ID' => $this->scheme->questions,
			'ACTIVE' => 'N',
			'ACTIVE_FROM' => ConvertTimeStamp($now, 'FULL')
		];

		// validate author
		if (!$author_id){
			if ($this->allowAnon){
				if (!filter_var($data['user_email'], FILTER_VALIDATE_EMAIL))
					return $this->error('Укажите корректный email-адрес');
				if (mb_strlen($data['user_name']) < 2)
					return $this->error('Представьтесь, пожалуйста');

				$question['PROPERTY_VALUES']['ANON_NAME'] = $data['user_name'];
				$question['PROPERTY_VALUES']['ANON_EMAIL'] = $data['user_email'];
				$question['PROPERTY_VALUES']['ANON_IP'] = $_SERVER['REMOTE_ADDR'];

			} else {
				return $this->error('Только для зарегистрированных');
			}
		} else {
			$question['PROPERTY_VALUES']['AUTHOR_USER'] = $author_id;
		}

		// validate common text fields
		if (mb_strlen($data['text']) < 20)
			return $this->error('Введите текст вопроса');
		$question['DETAIL_TEXT'] = $data['text'];

		if (mb_strlen($data['name']) < 5)
			return $this->error('Введите тему вопроса');
		$question['NAME'] = $data['name'];
		$question['CODE'] = Site::codegen(date('ms', $now).'-'.$question['NAME']);

		// validate section selection
		$section = $this->getQASection(null, $data['section']);
		if (!$section)
			return $this->error('Выберите тематику вопроса');
		$question['IBLOCK_SECTION_ID'] = $section['ID'];

		// select teams
		$teams = $this->getTeams(['COMPANIES' => ['PROPERTY_QA_SECTIONS' => $section['ID']]]);

		// place direct expert flag
		if (!empty($data['expert']) && $expert = arr::_($teams['EXPERTS'], $data['expert'])){
			// if expert selected directly - lock up
			$question['PROPERTY_VALUES']['DIRECT_EXPERT'] = $expert['ID'];
		}

		// configurable params
		if ($data['privacy'])
			$question['PROPERTY_VALUES']['AUTHOR_PRIVACY'] = $this->scheme->ib_prop_flags['AUTHOR_PRIVACY'];
		if ($data['smm'])
			$question['PROPERTY_VALUES']['SMM_POST'] = $this->scheme->ib_prop_flags['SMM_POST'];

		// create element
		$obElement = new CIBlockElement();
		if (!$question['ID'] = $obElement->Add($question)){
			$error = $obElement->LAST_ERROR;
			if (mb_strpos($error, 'уже существует'))
				$error = 'Вопрос с похожим заголовком уже существует. Пожалуйста, укажите другое название';
			return $this->error('Ошибка: '.$error);
		}

		// fetch element again to acquire correct link
		$arQuestion = $this->getQuestions(['ID' => $question['ID']], true);

		if ($this->moderateQuestions){
			// notify moderator
			$this->notifyModerator($arQuestion);
		} else {
			// send direct message
			$this->notifyExpertDirectly($arQuestion);
		}

		return $question['ID'];
	}

	public function putAnswer($author_id, $question_id, $data){
		$now = time();
		$answer = [
			'IBLOCK_ID' => $this->scheme->answers,
			'ACTIVE' => 'Y',
			'ACTIVE_FROM' => ConvertTimeStamp($now, 'FULL'),
			'PROPERTY_VALUES' => [
				'QUESTION' => $question_id
			],
		];

		// validate author
		if (!$author_id)
			return $this->error('Только для зарегистрированных');

		// validate common text fields
		if (mb_strlen($data['text']) < 20)
			return $this->error('Текст ответа слишком краток');
		$answer['DETAIL_TEXT'] = $data['text'];

		// validate answers thread
		$parent_id = (int)arr::_($data, 'parent_id', 0);
		if ($parent_id){
			if (!arr::_($this->getAnswers($question_id), $parent_id))
				return $this->error('Выберите ветку для ответа');
		}
		$answer['PROPERTY_VALUES']['PARENT'] = $parent_id;

		// is it expert's reply
		$expert = $this->getExperts(['PROPERTY_USER' => $author_id], false, false, true);
		if ($expert && $expert = reset($expert['EXPERTS'])){
			$answer['PROPERTY_VALUES']['AUTHOR_EXPERT'] = $expert['ID'];
		} else {
			$answer['PROPERTY_VALUES']['AUTHOR_USER'] = $author_id;
		}

		// get question
		$question = $this->getQuestions(['ID' => $question_id], true);

		$answer['NAME'] = Site::qaCode($question['IBLOCK_SECTION_ID'], $question['ID'])
			.'-'.$parent_id.'-'.$author_id;

		$obElement = new CIBlockElement();
		if (!($answer['ID'] = $obElement->Add($answer)))
			return $this->error($obElement->LAST_ERROR);

		return $answer['ID'];
	}

	public function notifyModerator($payload, $case = 'new_question'){
		if ($case == 'new_question'){
			$section = $this->getQASection(null, $payload['IBLOCK_SECTION_ID']);
			$author = $this->getUsers($payload['AUTHOR_USER'], true);

			$b = CEvent::Send($this->scheme->mail_events['QA_NOTIFY_MODERATOR'], SITE_ID, [
				'QUESTION_NAME' => $payload['NAME'],
				'QUESTION_MESSAGE' => $payload['DETAIL_TEXT'],
				'QUESTION_SECTION' => $section['NAME'],
				'QUESTION_AUTHOR' => $author['FULL_NAME'],
				'DIRECT_LINK' => sprintf('/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=%d&type=qa&ID=%d&lang=ru&find_section_section=%d&form_element_1_active_tab=cedit1',
					$payload['IBLOCK_ID'], $payload['ID'], $payload['IBLOCK_SECTION_ID'])
			], 'N');
			return $b;
		}
		return false;
	}

	public function notifyUser($payload, $case){
		if ($case == 'question_reject'){
			$section = $this->getQASection(null, $payload['IBLOCK_SECTION_ID']);
			$author = $this->getUsers($payload['AUTHOR_USER'], true);
			$review_note = CIBlockElement::GetProperty($payload['IBLOCK_ID'], $payload['ID'], [],
				['CODE' => 'NOTE'])->Fetch();
			if (!$review_note)
				$review_note = '&lt;не указана&gt;';

			$b = CEvent::Send($this->scheme->mail_events['QA_NOTIFY_REJECT'], SITE_ID, [
				'EMAIL_TO' => $author['EMAIL'],
				'QUESTION_NAME' => $payload['NAME'],
				'QUESTION_SECTION' => $section['NAME'],
				'QUESTION_AUTHOR' => $author['FULL_NAME'],
				'REJECT_MESSAGE' => $review_note
			], 'N');
			return $b;
		}
		return false;
	}

	public function notifyQuestionExperts($question){
		$section = $this->getQASection(null, $question['IBLOCK_SECTION_ID']);

		$experts = [];
		// if direct question
		if ($question['DIRECT_EXPERT']){
			$expert = $this->getExperts(['ID' => $question['DIRECT_EXPERT']], false, false, true);
			if (!$expert)
				$experts[] = $expert['EXPERTS'];
		}
		// if no direct expert - all from companies on section
		if (!$experts){
			$teams = $this->getTeams(['COMPANIES' => ['PROPERTY_QA_SECTIONS' => $section['ID']]]);
			$idExperts = [];
			if ($teams['EXPERTS']) foreach ($teams['EXPERTS'] as $expert){
				$idExperts[] = $expert['ID'];
			}
			if ($idExperts)
				$experts = $this->getExperts(['ID' => $idExperts], false, false, true)['EXPERTS'];
		}

		if (!$experts)
			return false;

		$bs = [];
		foreach ($experts as $expert){
			$bs[] = CEvent::Send($this->scheme->mail_events['QA_NOTIFY_EXPERT'], SITE_ID, [
				'EMAIL_TO' => $expert['USER']['EMAIL'],
				'QUESTION_NAME' => $question['NAME'],
				'QUESTION_MESSAGE' => $question['DETAIL_TEXT'],
				'QUESTION_PAGE' => $question['DETAIL_PAGE_URL'],
				'QUESTION_SECTION' => $section['NAME'],
				'EXPERT_NAME' => $expert['NAME'],
				'DIRECT_LINK' => $this->crossAuthLink($question['DETAIL_PAGE_URL'], $expert['USER']['ID'])
			], 'N');
		}
		return $bs;
	}

	public function notifyExpertDirectly($question){
		$section = $this->getQASection(null, $question['IBLOCK_SECTION_ID']);

		$experts = [];
		// if direct question
		if ($question['DIRECT_EXPERT']){
			$expert = $this->getExperts(['ID' => $question['DIRECT_EXPERT']], false, false, true);
			if ($expert)
				$experts = $expert['EXPERTS'];
		}

		// allow only direct messages
		if (!$experts)
			return false;

		$bs = [];
		foreach ($experts as $expert){
			$bs[] = CEvent::Send($this->scheme->mail_events['QA_MESSAGE_EXPERT_DIRECT'], SITE_ID, [
				'EMAIL_TO' => $expert['USER']['EMAIL'],
				'QUESTION_NAME' => $question['NAME'],
				'QUESTION_MESSAGE' => $question['DETAIL_TEXT'],
				'QUESTION_PAGE' => $question['DETAIL_PAGE_URL'],
				'QUESTION_SECTION' => $section['NAME'],
				'EXPERT_NAME' => $expert['NAME'],
				'AUTHOR_NAME' => $question['ANON_NAME'],
				'AUTHOR_EMAIL' => $question['ANON_EMAIL'],
			], 'N');
		}
		return $bs;
	}

	public function onQuestionUpdate($data){
		// check review state
		$prop_id = $this->scheme->ib_prop_id[$this->scheme->questions]['REVIEW_STATUS'];
		$review_state = arr::_($data['PROPERTY_VALUES'], $prop_id);
		if ($review_state)
			$review_state = $review_state['0']['VALUE'];

		if ($review_state){
			// if not empty and prev value is empty - process cases
			$lastProp = CIBlockElement::GetProperty($data['IBLOCK_ID'], $data['ID'], [],
				['CODE' => 'REVIEW_STATUS'])->Fetch();
			if (!$lastProp['VALUE']){
				$states = $this->scheme->ib_prop_list['REVIEW_STATUS'];
				$question = $this->getQuestions(['ID' => $data['ID']], true);
				if ($review_state == $states['ACCEPT']){
					$this->notifyQuestionExperts($question);
					CAdminMessage::ShowNote("Уведомления о создании вопроса отправлены экспертам");
				} else if ($review_state == $states['REJECT']){
					$this->notifyUser($question, 'question_reject');
					CAdminMessage::ShowNote("Отправлено уведомление пользователю");
				}
			}
		}
	}

	public function getTotalAnswers($author_expert){
		$n = 0;
		$ob = CIBlockElement::GetList([], [
			'IBLOCK_ID' => $this->scheme->answers,
			'PROPERTY_AUTHOR_EXPERT' => $author_expert
		]);
		while ($ar = $ob->Fetch())
			$n++;
		return $n;
	}

	public function getTeams($filter = []){
		// select companies
		$arCompanies = $this->getCompanies(arr::_($filter, 'COMPANIES', []));
		if (!$arCompanies)
			return [];

		// select related experts
		$filterExperts = array_merge(['PROPERTY_COMPANY' => array_keys($arCompanies)], arr::_($filter, 'EXPERTS', []));
		$expertCombo = $arCompanies
			? $this->getExperts($filterExperts, false, true)
			: ['EXPERTS' => [], 'COMPETENCIES' => []];

		// find and remove empty companies if experts filter was applied
		if (arr::_($filter, 'EXPERTS')){
			// no experts = no data
			if (!$expertCombo['EXPERTS'])
				$arCompanies = [];
			else {
				// fill company ids
				$idCompanies = [];
				foreach ($expertCombo['EXPERTS'] as $expert){
					if (!isset($idCompanies[$expert['COMPANY']]))
						$idCompanies[$expert['COMPANY']] = 1;
					$idCompanies[$expert['COMPANY']]++;
				}
				// find/remove empty
				$arCompanies = array_filter($arCompanies, function ($c) use ($idCompanies){
					if (isset($idCompanies[$c['ID']]))
						return $c;
					else return false;
				});
			}
		}

		return [
			'EXPERTS' => $expertCombo['EXPERTS'],
			'COMPANIES' => $arCompanies,
			'COMPETENCIES' => $expertCombo['COMPETENCIES']
		];
	}

	public function getExperts($filter = [], $expand_company = false, $expand_competencies = false, $expand_user = false){
		static $map_expert = [
			'ID', 'NAME', 'FULL_NAME', 'AVATAR',
			'POSITION@POSITION:VALUE',
			'COMPANY@COMPANY:VALUE',
			'COMPETENCIES@COMPETENCE:VALUE',
			'USER'
		];
		static $map_user = [
			'ID', 'LOGIN', 'EMAIL'
		];

		$arExperts = [];
		$idCompanies = [];
		$idCompetencies = [];
		// select experts
		$filterExperts = array_merge(['IBLOCK_ID' => $this->scheme->experts], $filter);
		$ob = CIBlockElement::GetList(self::$sort_asc, $filterExperts);
		while ($e = $ob->GetNextElement()){
			$item = array_merge($e->GetFields(), $e->GetProperties());
			if ($expand_company){
				$idCompanies[] = $item['COMPANY']['VALUE'];
			}
			if ($expand_competencies){
				$idCompetencies = array_merge($idCompetencies, $item['COMPETENCE']['VALUE']);
			}
			$item['FULL_NAME'] = $item['NAME'];
			$item['AVATAR'] = $item['PREVIEW_PICTURE'] ?
				CFile::GetPath($item['PREVIEW_PICTURE']) :
				$this->scheme->placeholder_avatar;

			if ($expand_user){
				$uid = $item['USER']['VALUE'];
				$user = $this->getUsers($uid, true);
				$item['USER'] = $user ? arr::mapify($map_user, $user) : [];
			} else {
				$item['USER'] = $item['USER']['VALUE'];
			}

			$arExperts[$item['ID']] = arr::mapify($map_expert, $item);
		}

		if (!$arExperts)
			return [];

		$data = [
			'EXPERTS' => $arExperts
		];

		if ($expand_company){
			$data['COMPANIES'] = $idCompanies ? $this->getCompanies(['ID' => $idCompanies]) : [];
		}

		if ($expand_competencies){
			$data['COMPETENCIES'] = $idCompetencies ? $this->getCompetencies(['ID' => $idCompetencies]) : [];
		}

		return $data;
	}

	public function getCompetencies($filter){
		static $map = [
			'ID', 'IBLOCK_ID', 'CODE', 'NAME'
		];
		$data = [];
		$filter = array_merge(['IBLOCK_ID' => $this->scheme->competencies], $filter);
		$ob = CIBlockElement::GetList(self::$sort_desc, $filter);
		while ($item = $ob->Fetch()){
			$data[$item['ID']] = arr::mapify($map, $item);
		}
		return $data;
	}

	public function getCompanies($filter){
		static $map = [
			'ID', 'NAME', 'IBLOCK_ID', 'CODE', 'DETAIL_TEXT', 'PREVIEW_TEXT', 'DETAIL_PAGE_URL',
			'LOGO', 'PHONES', 'WEBSITE', 'WEBSITE_URL',
			'QA_SECTIONS@QA_SECTIONS:VALUE'
		];
		$data = [];
		$filterCompanies = array_merge(['IBLOCK_ID' => $this->scheme->companies], $filter);
		$ob = CIBlockElement::GetList(self::$sort_desc, $filterCompanies, false);
		while ($e = $ob->GetNextElement()){
			$item = array_merge($e->GetFields(), $e->GetProperties());
			$item['LOGO'] = $item['DETAIL_PICTURE'] ?
				CFile::GetPath($item['DETAIL_PICTURE']) :
				$this->scheme->placeholder_avatar;
			$item['PHONES'] = [];
			if (count($item['CONTACT_PHONE']['VALUE'])){
				$item['PHONES'] = $item['CONTACT_PHONE']['VALUE'];
			}
			$item['WEBSITE'] = $item['CONTACT_WEB']['VALUE'];
			$item['WEBSITE_URL'] = 'http://'.$item['WEBSITE'].'/';

			$data[$item['ID']] = arr::mapify($map, $item);
		}
		return $data;
	}

	public function getEvent($filter){
		static $map = [
			'ID', 'IBLOCK_ID', 'CODE', 'NAME', 'PREVIEW_TEXT',
			'DETAIL_TEXT', 'BANNER@DETAIL_PICTURE',
			'COORD@COORD:VALUE', 'ADDRESS@ADDRESS:VALUE',
			'SCHEDULE@SCHEDULE:VALUE', 'REG_START@REG_START:VALUE', 'REG_END@REG_END:VALUE'
		];
		$data = [];
		if (!is_array($filter))
			$filter = ['CODE' => $filter];
		$filter = array_merge(['IBLOCK_ID' => $this->scheme->events, 'ACTIVE' => 'Y'], $filter);
		$ob = CIBlockElement::GetList(self::$sort_desc, $filter);
		if ($e = $ob->GetNextElement()){
			$item = array_merge($e->GetFields(), $e->GetProperties());
			$data = arr::mapify($map, $item);
		}
		return $data;
	}

	public function putEventGuest($event_id, $data){
		if (!$event_id)
			return $this->error('Выберите мероприятие');
		if (!$data['name'])
			return $this->error('Представьтесь, пожалуйста');
		if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
			return $this->error('Введите корректный email');


		$ticket = [
			'IBLOCK_ID' => $this->scheme->event_guests,
			'ACTIVE' => 'Y',
			'NAME' => $data['name']
		];

		$props = [
			'EVENT' => $event_id,
			'IS_PRIVATE' => $data['type'] == 0 ? $this->scheme->ib_prop_flags['IS_PRIVATE'] : '',
			'EMAIL' => $data['email'],
			'PHONE' => $data['phone']
		];

		if (!$props['IS_PRIVATE']){
			if (mb_strlen($data['company_name']) < 2 || mb_strlen($data['company_position']) < 2)
				return $this->error('Укажите сведения о вашей компании');

			$props['TEXT_COMPANY'] = $data['company_name'];
			$props['POSITION'] = $data['company_position'];
		}

		$ticket['PROPERTY_VALUES'] = $props;

		$obElement = new CIBlockElement();
		if (!$question['ID'] = $obElement->Add($ticket)){
			$error = $obElement->LAST_ERROR;
			return $this->error('Ошибка: '.$error);
		}

		return true;
	}

	public function getUsers($userIds = null, $single = false){
		global $USER;
		$userDefault = [
			'ID' => 0,
			'FULL_NAME' => '',
			'AVATAR' => $this->scheme->placeholder_avatar,
			'EXPERT' => false
		];

		if (!$userIds){
			$userIds = [$USER->GetID()];
		}
		if (!is_array($userIds)){
			$userIds = [$userIds];
		}
		$data = [];

		$obUser = CUser::GetList($_1 = 'ID', $_2 = 'ASC', ['ID' => implode('|', $userIds)]);
		while ($user = $obUser->GetNext()){
			$user['FULL_NAME'] = $user['NAME'].' '.$user['LAST_NAME'];
			//if (mb_strlen($user['FULL_NAME']) > 20)
			//	$user['FULL_NAME'] = mb_substr($user['FULL_NAME'], 0, mb_strpos($user['FULL_NAME'], ' ', 10));
			$user['AVATAR'] = $this->scheme->placeholder_avatar;
			if ($user['PERSONAL_PHOTO']){
				$image = CFile::GetPath($user['PERSONAL_PHOTO']);
				if ($image)
					$user['AVATAR'] = $image;
			}
			$data[$user['ID']] = $user;
		}

		foreach ($userIds as $id){
			if (!isset($data[$id]))
				$data[$id] = $userDefault;
		}

		return $single ? ($data[$userIds[0]]) : $data;
	}

	public function getCurrentUser($force = false){
		static $u = null;
		if ($u == null || $force){
			$u = $this->getUsers(null, true);
			$u['EXPERT'] = false;
			if ($u['ID']){
				$expert = $this->getExperts(['PROPERTY_USER' => $u['ID']], true);
				if ($expert){
					$u['EXPERT'] = reset($expert['EXPERTS']);
					$u['COMPANY'] = reset($expert['COMPANIES']);
				}
			}
		}
		return $u;
	}

	public function threadSeenAgent(){
		global $DB;
		// cleanup expired values
		$ob = $DB->Query("DELETE FROM ".$this->scheme->db_table_seen."
			WHERE timestamp < ".(strtotime($this->scheme->seen_expire)), true, __LINE__);
		if (!$ob){
			// create table
			$DB->Query("CREATE TABLE ".$this->scheme->db_table_seen." (
				`uid` INT NOT NULL,
				`tid` INT NOT NULL,
				`timestamp` INT NULL
			)", true, __LINE__);
		}
	}

	public function getThreadSeen($userId, $threadId = null){
		global $DB;
		$data = [];
		$q = "SELECT * FROM ".$this->scheme->db_table_seen." WHERE uid = '$userId'";
		if ($threadId)
			$q .= " AND tid = '$threadId'";
		$ob = $DB->Query($q, true, __LINE__);
		if ($ob)
			while ($ar = $ob->Fetch())
				$data[$ar['tid']] = $ar['timestamp'];

		return $threadId ? arr::_($data, $threadId) : $data;
	}

	public function putThreadSeen($userId, $threadId, $date = null){
		global $DB;
		if (!$date)
			$date = time();

		$ob = $DB->Query("UPDATE ".$this->scheme->db_table_seen."
			SET timestamp = '$date'
			WHERE uid = '$userId' AND tid = '$threadId';", true, __LINE__);

		if (!$ob->AffectedRowsCount()){
			$ob = $DB->Query("INSERT INTO ".$this->scheme->db_table_seen."
			(uid, tid, timestamp) VALUES ('$userId', '$threadId', '$date');", true, __LINE__);

			$this->threadSeenAgent();
		}
	}

	public function aggregateSearch($sources){
		$results = [];
		$ibGroup = [];
		$props = [];
		foreach ($sources as $s){
			if (!isset($ibGroup[$s['IBLOCK_ID']]))
				$ibGroup[$s['IBLOCK_ID']] = [];
			$ibGroup[$s['IBLOCK_ID']][$s['ID']] = $s;
		}

		foreach ($ibGroup as $ibId => $ibElements){
			$selector = ['IBLOCK_ID' => $ibId, 'ID' => array_keys($ibElements)];
			$selectorPath = '';
			switch (intval($ibId)){
				case $this->scheme->publications:
					$selectorPath = 'PUBLICATIONS';
					break;
				case $this->scheme->questions:
					$selectorPath = 'QUESTIONS';
					break;
				case $this->scheme->answers:
					$selectorPath = '_ANSWERS';
					$props = ['*', 'PROPERTY_QUESTION'];
					break;
				case $this->scheme->companies:
					$selectorPath = 'COMPANIES';
					break;
			}

			if ($selectorPath){
				$results[$selectorPath] = [];
				$ob = CIBlockElement::GetList(['SORT' => 'DESC'], $selector, false, false, $props);
				while ($ar = $ob->GetNext()){
					$ar['MATCH'] = $ibElements[$ar['ID']];
					$results[$selectorPath][] = $ar;
				}
			}

			if ($selectorPath == '_ANSWERS'){
				// Bubble-up answers to questions
				$results['ANSWERS'] = [];
				$qIds = [];
				$qaMap = [];
				foreach ($results[$selectorPath] as $ans){
					$qIds[] = $ans['PROPERTY_QUESTION_VALUE'];
					$qaMap[$ans['PROPERTY_QUESTION_VALUE']] = $ans['MATCH'];
				}
				$ob = CIBlockElement::GetList([], ['IBLOCK_ID' => $this->scheme->questions, 'ID' => array_unique($qIds)]);
				while ($ar = $ob->GetNext()){
					$ar['MATCH'] = $qaMap[$ar['ID']];
					$results['ANSWERS'][] = $ar;
				}
			}
		}
		return $results;
	}

	static function extractBanners($arResult){
		$data = arr::_($arResult, 'BANNERS_PROPERTIES');
		if (!$data)
			return false;

		$data = [];

		foreach ($arResult['BANNERS'] as $i => $banner){
			preg_match('/href="(.+?)".+src="(.+?)"/', $banner, $matches);
			$item = [
				'LINK' => $matches[1],
				'IMAGE' => $matches[2]
			];
			$item['HTML'] = str_replace(
				array('#IMAGE#', '#LINK#', '#NAME#', '#TARGET#'),
				array(
					$item['IMAGE'],
					'href="'.$item['LINK'].'"',
					$arResult['BANNERS_PROPERTIES'][$i]['NAME'],
					'target="'.$arResult['BANNERS_PROPERTIES'][$i]['URL_TARGET'].'" ',
				),
				$arResult['BANNERS_PROPERTIES'][$i]['CODE']
			);
			$data[] = $item;
		}

		return $data;
	}

	static function codegen($name){
		return CUtil::translit($name, "ru", [
			'max_len' => 100,
			'change_case' => 'L',
			'replace_space' => '-',
			'replace_other' => '-',
			'delete_repeat_replate' => true,
			'use_google' => false
		]);
	}

	static function qaCode($section_id, $element_id){
		return sprintf('ID#%02d-%03d', $section_id, $element_id);
	}

	static function qaExpertSectionLink($code = ''){
		return '/club/'.($code ? $code.'/' : '');
	}

	static function setSefParams($item, $is_section = false){
		global $APPLICATION;

		$className = '\\Bitrix\\Iblock\\InheritedProperty\\'.($is_section ? 'SectionValues' : 'ElementValues');

		$ob_item_prop = new $className($item['IBLOCK_ID'], $item['ID']);
		if ($item_prop = $ob_item_prop->getValues()){
			$APPLICATION->SetTitle(
				arr::_($item_prop,
					$is_section ? 'SECTION_PAGE_TITLE' : 'ELEMENT_PAGE_TITLE',
					$item['NAME'])
			);
			$APPLICATION->SetPageProperty('title', $APPLICATION->GetTitle());
		}
	}

	static function date($string, $format = 'DETAIL'){
		static $date_rel = [
			"tomorrow" => "tomorrow",
			"today" => "today",
			"yesterday" => "yesterday",
			"d" => 'j F',
			"" => 'j F Y',
		];
		$ts = strtotime($string);

		if ($format == 'DATE_RELATIVE'){
			return strtolower(FormatDate($date_rel, $ts));
		} else if ($format == 'FULL_RELATIVE'){
			$ts = strtotime($string);
			return strtolower(FormatDate($date_rel, $ts)).' в '.date('H:i', $ts);
		} else if ($format == 'DETAIL'){
			$format = 'd.m.Y H:i:s';
		}
		return date($format, $ts);
	}

	static function vendetta($name){
		$out = [];
		$words = explode(' ', $name);
		foreach ($words as $word){
			if (mb_strlen($word) > 2){
				$out[] = mb_substr($word, 0, 1).'***'.mb_substr($word, -1, 1);
			} else {
				$out[] = $word;
			}
		}
		return trim(implode(' ', $out));
	}

	static function vendeface($letters){
		if ($space = mb_strpos($letters, ' ')){
			$letters = mb_substr($letters, 0, 1).mb_substr($letters, $space + 1, 1);
		}
		return '/ajax/face.php?q='.mb_substr($letters, 0, 2);
	}

	static function pornomize($user){
		$user['FULL_NAME'] = Site::vendetta($user['FULL_NAME']);
		$user['AVATAR'] = Site::vendeface($user['FULL_NAME']);
		return $user;
	}

	static function reset(){
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
	}

	static function notFound(){
		global $APPLICATION;
		CHTTP::SetStatus('404 Not Found');
		$APPLICATION->SetTitle('Страница не найдена либо доступ к ней ограничен');
		include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/content/404.html';
	}

}

class SiteScheme {
	const ib_questions = 1;
	var $questions = SiteScheme::ib_questions;
	var $answers = 2;
	var $companies = 3;
	var $experts = 7;
	var $competencies = 4;
	var $faq = 6;
	var $publications = 5;
	var $events = 8;
	var $event_guests = 9;
	var $pub_news = 8;
	var $pub_articles = 9;
	var $pub_interviews = 10;
	var $placeholder_avatar = '/media/avatarka.png';
	var $personal_area = '/personal/';
	var $db_table_seen = 'ns_sd_seen';
	var $seen_expire = '6 months ago';
	var $ib_prop_flags = [
		'SMM_POST' => 9,
		'AUTHOR_PRIVACY' => 3,
		'IS_PRIVATE' => 12
	];
	var $ib_prop_list = [
		'REVIEW_STATUS' => [
			'ACCEPT' => 10, 'REJECT' => 11
		]
	];
	var $ib_prop_id = [
		SiteScheme::ib_questions => [
			'REVIEW_STATUS' => 1,
			'REVIEWED_BY' => 2,
			'NOTES' => 3
		]
	];
	var $mail_events = [
		'QA_NOTIFY_EXPERT' => 'QA_NEW_QUESTION_EXPERT',
		'QA_MESSAGE_EXPERT_DIRECT' => 'QA_NEW_QUESTION_EXPERT_DIRECT',
		'QA_NOTIFY_MODERATOR' => 'QA_NEW_QUESTION_MODERATOR',
		'QA_NOTIFY_REJECT' => 'QA_QUESTION_REJECT'
	];
}

class txt {
	static function cond($condition, $on_true = true, $on_false = null){
		return $condition ? $on_true : $on_false;
	}

	static function ab($first, $second){
		return $first ? $first : $second;
	}

	static function pre($args){
		$args = func_get_args();
		foreach ($args as $arg)
			echo '<pre>'.print_r($arg, 1).'</pre>';
	}

	static function plural($n, $forms){
		return $n % 10 == 1 && $n % 100 != 11
			? $forms[0]
			: ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20)
				? arr::_($forms, 1, $forms[0])
				: arr::_($forms, 2, $forms[0])
			);
	}
}

class arr {
	static function _($array, $key, $default = null){
		return isset($array[$key]) ? $array[$key] : $default;
	}

	static function mapify($schema, $source){
		$out = [];
		foreach ($schema as $row){
			$alias = explode('@', $row);
			$key_out = $alias[0];
			$key_way = $alias[0];
			if (count($alias) > 1)
				$key_way = $alias[1];
			$path = explode(':', $key_way);
			$value = null;
			switch (count($path)){
				case 1:
					$value = arr::_($source, $path[0]);
					break;
				case 2:
					$value = arr::_($source[$path[0]], $path[1]);
					break;
				default:
					$value = arr::_($source[$path[0]][$path[1]], $path[2]);
					break;
			}
			$out[$key_out] = $value;
		}
		return $out;
	}

	static function plain_tree_keys($source, $key_id, $key_parent){
		$parent = 0;
		$out_keys = [];
		$stack = [];

		array_push($stack, $parent);
		do {
			$pk = array_pop($stack);
			$_c = count($stack);
			foreach ($source as $i){
				if ($i[$key_parent] == $pk && !isset($out_keys[$i[$key_id]])){
					$out_keys[$i[$key_id]] = count($stack);
					array_push($stack, $pk);
					array_push($stack, $i[$key_id]);
					break;
				}
			}
			if ($pk != $parent && count($stack) == $_c){
				$out_keys[$pk] = count($stack);
			}
		} while (count($stack));

		return $out_keys;
	}
}