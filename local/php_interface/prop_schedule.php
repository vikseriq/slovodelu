<?

class CCustomTypeSchedule {
	function GetUserTypeDescription(){
		return array(
			'PROPERTY_TYPE' => 'S',
			'USER_TYPE' => 'schedule',
			'DESCRIPTION' => 'Расписание',
			'GetAdminListViewHTML' => array('CCustomTypeSchedule', 'GetAdminListViewHTML'),
			'GetPropertyFieldHtml' => array('CCustomTypeSchedule', 'GetPropertyFieldHTML'),
			'ConvertToDB' => array('CCustomTypeSchedule', 'ConvertToDB'),
			'ConvertFromDB' => array('CCustomTypeSchedule', 'ConvertFromDB')
		);
	}

	function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName){
		return 'Расписание';
	}

	function GetPropertyFieldHTML($arProperty, $value, $cName){
		$ui['TIME'] = "<input type='text' name='$cName[VALUE][TIME]' value=\"{$value['VALUE']['TIME']}\">";
		$ui['DESC'] = "<textarea style='width: 100%;' name='$cName[VALUE][DESC]'>{$value['VALUE']['DESC']}</textarea>";

		$html = "
                <table style='margin: 10px;'>
                    <tr>
                        <td>Время:</td> <td width='70%'> $ui[TIME] </td>
                    </tr>
                    <tr>
                        <td>Описание:</td> <td> $ui[DESC] </td>
                    </tr>
                </table>
            ";

		return $html;
	}

	function ConvertToDB($prop, $value){
		if (empty($value['VALUE']['TIME']) && empty($value['VALUE']['DESC']))
			return array();

		return array('VALUE' => serialize($value['VALUE']));
	}

	function ConvertFromDB($prop, $value){
		return array('VALUE' => unserialize($value['VALUE']));
	}
}

AddEventHandler('iblock', 'OnIBlockPropertyBuildList', array('CCustomTypeSchedule', 'GetUserTypeDescription'), 100, __FILE__);