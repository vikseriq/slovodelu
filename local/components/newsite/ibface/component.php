<? if (!defined('B_PROLOG_INCLUDED')) die('hard');
/**
 * Universal IBlock controller-model processor
 *
 * @author w
 */

$dataType = arr::_($arParams, 'DATA_TYPE');

$arSort = [arr::_($arParams, 'SORT_BY', 'SORT') => arr::_($arParams, 'SORT_ASC', false) ? 'ASC' : 'DESC'];
$arFilter = ['ACTIVE' => 'Y', 'IBLOCK_ID' => arr::_($arParams, 'DATA_IBLOCK_ID')];
if ($extFilter = arr::_($arParams, 'DATA_FILTER')){
	$arFilter = array_merge($arFilter, $extFilter);
}
$allProps = arr::_($arParams, 'DATA_FETCH_PROPS', false);
$iterationSelector = $allProps ? 'GetNextElement' : 'GetNext';
$maxQty = arr::_($arParams, 'QUANTITY', 0);
$i = 0;
$data = [];

switch ($dataType){
	case 'ELEMENTS':
		$ob = CIBlockElement::GetList($arSort, $arFilter);
		while ($ar = $ob->$iterationSelector()){
			if ($allProps)
				$ar = array_merge($ar->GetFields(), $ar->GetProperties());
			$data[$ar['ID']] = $ar;
			if ($maxQty && ++$i >= $maxQty) break;
		}
		break;
	case 'SECTIONS':
		$countEls = arr::_($arParams, 'SECTIONS_ELEMENTS_COUNT', false);
		if ($countEls){
			$arFilter['CNT_ACTIVE'] = true;
			$arFilter['GLOBAL_ACTIVE'] = 'Y';
		}
		$ob = CIBlockSection::GetList($arSort, $arFilter, $countEls);
		while ($ar = $ob->GetNext()){
			$data[$ar['ID']] = $ar;
			if ($maxQty && ++$i >= $maxQty) break;
		}
		break;
	default:
		break;
}

$arResult = ['DATA' => $data];

$this->IncludeComponentTemplate();