<? defined('B_PROLOG_INCLUDED') || die('hard');
$site = Site::get();
$arFilter = [];

$arParams['WIDGET_EXPAND'] = arr::_($_GET, 'widget');
$arParams['COMPETENCE_CHECKED'] = arr::_($_POST, 'competence');

if ($competency_filter = arr::_($arParams, 'COMPETENCE')){
	$arFilter['EXPERTS'] = ['PROPERTY_COMPETENCE' => $competency_filter];
}
if ($section_id = arr::_($arParams, 'QA_SECTION_ID')){
	$arFilter['COMPANIES'] = ['PROPERTY_QA_SECTIONS' => $section_id];
}
if ($company_id = arr::_($arParams, 'QA_COMPANY_ID')){
	$arFilter['COMPANIES'] = ['ID' => $company_id];
	$arParams['WIDGET_EXPAND'] = true;
	$arParams['COMPANY_EXPAND'] = true;
}
$arResult = $site->getTeams($arFilter);

if ($company_id){
	$arParams['COMPETENCE_CHECKED'] = array_keys($arResult['COMPETENCIES']);
}

if (arr::_($arParams, 'EXPERT_TOTAL_ANSWERS', false)){
	foreach ($arResult['EXPERTS'] as &$expert){
		$expert['TOTAL_ANSWERS'] = $site->getTotalAnswers($expert['ID']);
	}
}

$this->IncludeComponentTemplate();