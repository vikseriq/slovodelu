<? defined('B_PROLOG_INCLUDED') || die('hard');
if (!$arResult['COMPANIES'])
	return;
$i = 0;
$n = count($arResult['COMPANIES']);
$iMax = 4;
$base = reset($arResult['COMPANIES'])['LIST_PAGE_URL'];
?>
<div class="experts-panel">
	<div class="title text-center">
		<h3>Эксперты раздела</h3>
	</div>
	<ul class="item-list padding bottom-border">
		<? foreach ($arResult['COMPANIES'] as $item):
			if ($i++ >= $iMax) break;
			?>
			<li class="text-center">
				<a href="<?=$item['DETAIL_PAGE_URL']?>"><img src="<?=$item['LOGO']?>"/></a>
				<h4><?=$item['NAME']?></h4>

				<p class="basic-text"><?=$item['PREVIEW_TEXT']?></p>
				<a href="<?=$item['DETAIL_PAGE_URL']?>" class="basic-text bottom-margin">Подробнее</a>
			</li>
		<? endforeach ?>
		<? if ($n > $iMax): ?>
			<li class="top-padding">
				<a href="<?=$base?>" class="btn btn-default btn-block">Все эксперты</a>
			</li>
		<? endif ?>
	</ul>
</div>
