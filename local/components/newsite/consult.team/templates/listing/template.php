<? defined('B_PROLOG_INCLUDED') || die('hard');
$i = 0;
$n = count($arResult['COMPANIES']);
$iMax = arr::_($arParams, 'PER_PAGE', 5);
$pager_id = uniqid('pager_');
$company_expand = arr::_($arParams, 'COMPANY_EXPAND');
?>
<div class="inner js-experts-list">
	<? if (!$arResult['COMPANIES']):
		if (!arr::_($arParams, 'COMPETENCE')):
			?>
			<button class="btn btn-link btn-block">
				Нет активных экспертов в этой области.
			</button>
		<? else: ?>
			<button class="btn btn-link btn-block">
				Нет активных компаний с компетенциями в запрошенных областях.
			</button>
		<? endif;
	else: ?>
		<div class="js-ipage" data-pager="<?=$pager_id?>">
			<? foreach ($arResult['COMPANIES'] as $id => $company):
				$page_id = ceil(++$i / $iMax);

				$expander = '<a href="#company'.$id.'" class="basic-text btn btn-link btn-block collapsed" data-toggle="collapse">
			<span class="show-on">Свернуть
				<span class="btn-accessory glyphicon glyphicon-chevron-up"></span>
			</span>
			<span class="show-off">Подробнее
				<span class="btn-accessory glyphicon glyphicon-chevron-down"></span>
			</span></a>';
				?>
				<div class="content <?=txt::cond($page_id > 1, 'hidden')?>" data-page="<?=$page_id?>">
					<div class="row company-card <?=txt::cond($company_expand, 'expanded')?>">
						<div class="col-xs-4 business-card text-center">
							<img src="<?=$company['LOGO']?>" style="width: 129px">
							<? if ($company['PHONES']): ?>
								<div class="phone">
									<? foreach ($company['PHONES'] as $phone): ?>
										<span class="basic-text"><?=$phone?></span>
									<? endforeach ?>
								</div>
							<? endif ?>
							<? if ($company['WEBSITE']): ?>
								<a href="<?=$company['WEBSITE_URL']?>" target="_blank"><?=$company['WEBSITE']?></a>
							<? endif ?>
						</div>
						<div class="col-xs-8 content">
							<h2><?=$company['NAME']?></h2>

							<div class="basic-text"><?=$company['DETAIL_TEXT']?></div>
						</div>
					</div>
					<ul id="company<?=$id?>" class="<?=txt::cond(!$company_expand, 'collapse')?> experts-group">
						<? if (!$company_expand): ?>
							<li><?=$expander?></li>
						<? endif ?>
						<?
						$need_title = true;
						foreach ($arResult['EXPERTS'] as $expert):
							if ($expert['COMPANY'] != $company['ID'])
								continue;
							?>
							<li class="experts-card row">
								<div class="col-xs-4 business-card text-center">
									<? if ($need_title): $need_title = false; ?><span
										class="psevdo-h3">Эксперты компании</span><? endif; ?>
									<div class="img-circle"
										 style="background-image: url('<?=$expert['AVATAR']?>');"></div>
									<br/><br/>
									<button class="btn btn-primary btn-block" data-toggle="modal"
											data-target="#modal-base"
											data-src="/ajax/question.php?expert=<?=$expert['ID']?>&section_id=<?=$arParams['QA_SECTION_ID']?>">
										Задать вопрос
									</button>
								</div>
								<div class="col-xs-8 content">
									<div class="title">
										<p class="secondary-text"><?=$expert['NAME']?></p>

										<p class="secondary-text"><i><?=$expert['POSITION']?></i></p>
									</div>
									<p class="basic-text">Отвечает на вопросы:</p>
									<ul class="basic-text">
										<? foreach ($expert['COMPETENCIES'] as $cpid): ?>
											<li><?=$arResult['COMPETENCIES'][$cpid]['NAME']?></li>
										<? endforeach ?>
									</ul>
								</div>
							</li>
						<? endforeach ?>
					</ul>
					<? if (!$company_expand) echo $expander; ?>
				</div>
			<? endforeach ?>
		</div>
	<? endif ?>
	<? if ($n && $i > $iMax): ?>
		<button class="btn btn-link btn-block bottom-border js-ipage-next"
				data-pager="<?=$pager_id?>" data-page="1">Показать еще
			<span class="accessory-refresh"></span>
		</button>
		<script>Siteframe.inlinePager.init();</script>
	<? endif ?>
</div>
