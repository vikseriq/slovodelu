<? defined('B_PROLOG_INCLUDED') || die('hard');
if (!$arResult['COMPETENCIES'])
	return;
$checked = arr::_($arParams, 'COMPETENCE_CHECKED');
$n = count($arResult['COMPETENCIES']);
$i = 0;
$iMax = arr::_($arParams, 'WIDGET_EXPAND') ? $n : 5;
?>
<div class="block bottom-border">
	<div class="title">
		<h3>Прикладная область</h3>
	</div>
	<div class="content js-competency-holder" data-section="<?=$arParams['QA_SECTION_ID']?>"
		 data-target=".js-experts-list">
		<form method="post" action="<?=$APPLICATION->GetCurPageParam("widget=1")?>">
			<? foreach ($arResult['COMPETENCIES'] as $c):
				if ($i++ >= $iMax) break;
				?>
				<div class="checkbox">
					<input id="check_<?=$c['ID']?>" name="competence[]" type="checkbox" value="<?=$c['ID']?>"
						<?=txt::cond(in_array($c['ID'], $checked), 'checked')?>
						<?=txt::cond(arr::_($arParams, 'COMPANY_EXPAND'), 'disabled')?>>
					<label class="basic-text" for="check_<?=$c['ID']?>"><?=$c['NAME']?></label>
				</div>
			<? endforeach ?>
			<? if ($n > $iMax): ?>
				<button type="submit" class="btn btn-default btn-block">Все области (<?=$n?>)</button>
			<? endif ?>
		</form>
	</div>
</div>
<script>BX.addCustomEvent('onAjaxSuccess', competency_widget);</script>
