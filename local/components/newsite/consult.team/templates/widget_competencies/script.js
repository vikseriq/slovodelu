var competency_widget = function(){
	var holder = $('.js-competency-holder');
	holder.find('input[type=checkbox]').change(function(){
		var filter = [];
		holder.find('input:checked').each(function(_, e){
			filter.push(e.value);
		});
		$.post('/ajax/experts.php?section=' + holder.data('section'), {
			'filter': filter
		})
			.done(function(data){
				var target = $(holder.data('target'));
				var fresh = $(data);
				target.html(fresh.html());
			});
	});
};

$(document).ready(competency_widget);
