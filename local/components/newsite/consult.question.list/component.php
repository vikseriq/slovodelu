<? if (!defined('B_PROLOG_INCLUDED')) die('hard');
$site = Site::get();
$me = $site->getCurrentUser();

$arParams['PER_PAGE'] = arr::_($arParams, 'PER_PAGE', 10);

$section_id = arr::_($arParams, 'QA_SECTION_ID', 0);
$section = $site->getQASection(null, $section_id);
$q_filter = $site->getAccessFilter('question_list', $section_id);

if ($section_id)
	$q_filter['SECTION_ID'] = $section_id;

$questions = $site->getQuestions($q_filter);
$idUsers = [];
foreach($questions as $q){
	$idUsers[] = $q['AUTHOR_USER'];
}

$arResult = [
	'SECTION' => $section,
	'QUESTIONS' => $questions,
	'USERS' => $site->getUsers($idUsers)
];

$this->IncludeComponentTemplate();