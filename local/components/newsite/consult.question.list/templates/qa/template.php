<? if (!defined('B_PROLOG_INCLUDED')) die('hard'); ?>
<div class="member-questions">
	<div class="box-title">
		<h2>Вопросы участников</h2>
	</div>
	<? if (count($arResult['QUESTIONS'])): ?>
		<? if (count($arResult['QUESTIONS']) > $arParams['PER_PAGE']): ?>
			<div class="question-form">
				<form method="post" action="/ajax/questions.php">
					<div class="form-group basic-text input-group">
						<input type="text" class="form-control" placeholder="Поиск по вопросам"/>
						<span class="input-group-btn">
							<button class="btn btn-default">Найти</button>
						</span>
					</div>
				</form>
			</div>
		<? endif ?>

		<ul class="member-questions-list bottom-border">
			<? foreach ($arResult['QUESTIONS'] as $q):
				$user = $arResult['USERS'][$q['AUTHOR_USER']];
				if ($q['AUTHOR_PRIVACY'])
					$user = Site::pornomize($user);
				?>
				<li>
					<a href="<?=$q['DETAIL_PAGE_URL']?>" class="clearfix">
						<span class="img-circle pull-left text-center"
							  style="background-image: url('<?=$user['AVATAR']?>')"></span>

						<div class="pull-left question">
							<p class="main-style-text"><?=$q['NAME']?></p>

							<p class="secondary-text">
								<span><?=Site::date($q['DATE_CREATE'])?></span>
								<span><?=$user['FULL_NAME']?></span>
							</p>
						</div>
					</a>
				</li>
			<? endforeach ?>
		</ul>
		<!--<a href="#" class="btn btn-link btn-block bottom-border">
			Показать еще
			<span class="accessory-refresh"></span>
		</a>-->
	<? else: ?>
		<div class="box-title text-muted">Нет решенных вопросов на эту тему.
			<? if (Site::get()->canPutQuestion()): ?>
				<a href="#" data-toggle="modal" data-target="#modal-base"
				   data-src="/ajax/question.php?section=<?=$arResult['SECTION']['CODE']?>">
					Задать свой вопрос экспертам.</a>
			<? endif ?>
		</div>
	<? endif ?>
</div>
