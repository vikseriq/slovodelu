<?
if ($user['ID']):
	$avatar = $user['AVATAR'];
	if ($user['EXPERT']):
		$avatar = $user['EXPERT']['AVATAR'];
		$APPLICATION->AddHeadScript('/local/templates/sd/assets/js/summernote.min.js');
		$APPLICATION->AddHeadScript('/local/templates/sd/assets/js/summernote-ru-RU.js');
		$APPLICATION->SetAdditionalCSS('/local/templates/sd/assets/css/summernote.css');
	endif;
	?>
	<script>BX.addCustomEvent('onAjaxSuccess', qa_thread_chat);</script>
	<div class="question-form reply-form clearfix bottom-border">
		<? if ($arResult['ON_REPLY']['MESSAGE']): ?>
		<div class="message alert alert-<?=txt::cond($arResult['ON_REPLY']['TYPE'] == 'OK', 'success', 'danger')?>">
			<?=$arResult['ON_REPLY']['MESSAGE']?>
		</div>
		<? endif ?>
		<span class="img-circle pull-left" style="background-image: url('<?=$avatar?>');"></span>

		<div class="margin-form">
			<div class="message"></div>
			<form method="post" action="">
				<input type="hidden" name="thread" value="<?=$q['ID']?>"/>
				<input type="hidden" name="reply_to" value=""/>
				<div class="form-group basic-text">
					<textarea name="text" class="form-control summer-editable" rows="3" placeholder="Комментарий"></textarea>
				</div>
				<button type="submit" class="btn btn-primary js-do-reply">Отправить</button>
			</form>
		</div>
	</div>
<? else: ?>
	<div class="question-form clearfix bottom-border">
		<a href="<?=Site::get()->scheme->personal_area?>" class="btn btn-primary btn-block">
			Войти, чтобы ответить экспертам</a>
	</div>
<? endif ?>