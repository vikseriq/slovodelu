<?
$q = $arResult['QUESTION'];
Site::setSefParams($q);
$APPLICATION->AddChainItem($arResult['SECTION']['NAME'], $arResult['SECTION']['LINK']);
$APPLICATION->AddChainItem('Вопрос');
$APPLICATION->AddChainItem(NAVCHAIN_BACK.' К списку вопросов', $arResult['SECTION']['LINK']);

$answers = $arResult['ANSWERS'];
$answers_tree = arr::plain_tree_keys($arResult['ANSWERS'], 'ID', 'PARENT_ID');
$experts = $arResult['EXPERTS'];
$author = $arResult['USERS'][$q['AUTHOR_USER']];
if ($q['AUTHOR_PRIVACY']){
	$author['FULL_NAME'] = Site::vendetta($author['FULL_NAME']);
	$author['AVATAR'] = Site::vendeface($author['FULL_NAME']);
}
$user = Site::get()->getCurrentUser();

?>
<div class="container">
	<div class="experts-box">
		<div class="row">
			<div class="col-xs-9">
				<h1><?=$q['NAME']?></h1>

				<div class="inner">
					<ul class="question-chat">
						<li class="question bg-author clearfix">
							<span class="img-circle" style="background-image: url('<?=$author['AVATAR']?>');"></span>

							<div class="dialog basic-text">
								<div class="title">
									<h4 class="name"><?=$author['FULL_NAME']?></h4>
									<time class="secondary-text"
										  title="<?=Site::date($q['DATE_CREATE'], 'DETAIL')?>"><?=Site::date($q['DATE_CREATE'], 'FULL_RELATIVE')?></time>
									<? if ($user['ID']): ?>
										<a href="#reply" data-id="<?=$a['ID']?>" class="answer-reply js-reply">Ответить</a>
									<? endif ?>
								</div>
								<?=nl2br($q['DETAIL_TEXT'])?>

							</div>
						</li>
						<? if ($answers_tree) foreach ($answers_tree as $id => $level):
							$a = $answers[$id];
							$is_expert = $a['AUTHOR_EXPERT'];
							if ($is_expert){
								$replier = $experts['EXPERTS'][$a['AUTHOR_EXPERT']];
								$bg = 'bg-expert';
								$a['DETAIL_TEXT'] = html_entity_decode($a['DETAIL_TEXT']);
							} else {
								$replier = $arResult['USERS'][$a['AUTHOR_USER']];
								$bg = 'bg-user';
								if ($replier['ID'] == $author['ID']){
									// apply author's privacy settings
									$bg = 'bg-author';
									$replier = $author;
								}
							}
							?>
							<li class="answer <?=$bg?> level-<?=$level?> clearfix">
							<span class="img-circle" data-toggle="popup" data-target="#member_<?=$id?>"
								  style="background-image: url('<?=$replier['AVATAR']?>');"></span>
								<? if ($is_expert):
									$company = $experts['COMPANIES'][$replier['COMPANY']['VALUE']];
									?>
									<div id="member_<?=$id?>" class="popup clearfix">
										<div class="accessory"></div>

										<div class="title pull-left">
											<h4>Эксперт компании <a
													href="<?=$company['DETAIL_PAGE_URL']?>"><?=$company['NAME']?></a>
											</h4>

											<p class="basic-text"><?=$replier['POSITION']?></p>
										</div>
										<img class="pull-left" src="<?=$company['LOGO']?>"/>

										<button type="button" class="btn btn-close">
										</button>
									</div>
								<? endif ?>
								<div class="dialog basic-text">
									<div class="title">
										<h4 class="name" data-toggle="popup" data-target="#member_<?=$id?>">
											<?=$replier['FULL_NAME']?></h4>
										<time class="secondary-text"
											  title="<?=Site::date($a['DATE_CREATE'], 'DETAIL')?>"><?=Site::date($a['DATE_CREATE'], 'FULL_RELATIVE')?></time>
										<? if ($user['ID']): ?>
											<a href="#reply" data-id="<?=$a['ID']?>" class="answer-reply js-reply">Ответить</a>
										<? endif ?>
									</div>
									<?=$a['DETAIL_TEXT']?>
								</div>
							</li>
						<? endforeach ?>
					</ul>
					<? if (!$q['DISABLE_REPLY']) include __DIR__.'/reply_form.php'; ?>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="left-panel margin">
					<div class="spacer">
						<? if ($user['ID'] && !$user['EXPERT']): ?>
							<button class="btn btn-default btn-block btn-lg" data-toggle="modal"
									data-target="#modal-base"
									data-src="/ajax/question.php?section=<?=$arResult['SECTION']['CODE']?>">
								Задать вопрос
							</button>
						<? endif ?>
					</div>
					<div class="question-author text-center bottom-border">
						<div class="title">
							<h3>Спрашивает</h3>
						</div>
						<span class="img-circle" style="background-image: url('<?=$author['AVATAR']?>');"></span>
						<h4><?=$author['FULL_NAME']?></h4>
					</div>
					<div class="experts-panel">
						<div class="title text-center">
							<h3>Отвечают эксперты</h3>
						</div>
						<ul class="item-list bottom-border">
							<? foreach ($experts['COMPANIES'] as $id => $company): ?>
								<li class="text-center">
									<a href="<?=$company['DETAIL_PAGE_URL']?>">
										<img src="<?=$company['LOGO']?>">
									</a>
									<h4><?=$company['NAME']?></h4>

									<p class="basic-text"><?=$company['PREVIEW_TEXT']?></p>
									<a href="#company_expert_<?=$id?>" class="basic-text collapsed bottom-margin"
									   data-toggle="collapse"
									   aria-expanded="false">Подробнее</a>

									<div id="company_expert_<?=$id?>" class="company-experts-list collapse">
										<ul class="company-experts">
											<? foreach ($experts['EXPERTS'] as $expert):
												if ($expert['COMPANY'] != $id)
													continue;
												?>
												<li>
												<span class="img-circle"
													  style="background-image: url('<?=$expert['AVATAR']?>');"></span>
													<h4><?=$expert['NAME']?></h4>

													<p class="basic-text"><?=$expert['POSITION']?></p>
												</li>
											<? endforeach ?>
										</ul>
										<a href="#company_expert_<?=$id?>" class="basic-text collapsed btn-block"
										   data-toggle="collapse"
										   aria-expanded="false">Свернуть<span
												class="glyphicon glyphicon-chevron-up btn-accessory"></span></a>
									</div>

								</li>
							<? endforeach; ?>
						</ul>
					</div>
					<? $APPLICATION->IncludeComponent(
						"bitrix:advertising.banner",
						"raw",
						array(
							"TYPE" => "SECTION_RIGHT",
							"CACHE_TYPE" => "A",
							"NOINDEX" => "N",
							"CACHE_TIME" => "3600",
							"QUANTITY" => "1"
						),
						false
					); ?>
				</div>

			</div>
		</div>
	</div>
</div>