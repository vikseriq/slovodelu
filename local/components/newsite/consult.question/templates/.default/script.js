var qa_thread_chat = function(){
	// reply links: form scroll
	$('.js-reply').click(function(){
		var replyTo = $(this).attr('data-id');
		Siteframe.scrollTo($('.reply-form'), function(){
			$('.reply-form input[name=reply_to]').val(replyTo);
			$('.reply-form textarea').focus();
		});
	});

	// reply form handling
	$('.js-do-reply').click(function(){
		// basic content validation
		var form = $(this).closest('form');
		var textbox = form.find('textarea');
		if (!textbox.val() || textbox.val().length < 10){
			textbox.css('border-color', '#a94442');
			var note = form.find('.note-editable');
			if (note)
				note.css('backgroun-color', textbox.css('background-color'));
			return false;
		}
		// pass to BX.ajax
		$('.reply-form').css('opacity', 0.8);
		//$(this).attr('disabled', 'disabled');
		return true;
	});

	// editor
	if (typeof $.fn.summernote != 'undefined'){
		var h = 150;
		$('.summer-editable').summernote({
			lang: 'ru-RU',
			dialogsFade: true,
			height: h,
			maxHeight: 2 * h,
			minHeight: h,
			toolbar: [
				['style', ['bold', 'italic', 'underline', 'clear']],
				['insert', ['link']]
			]
		});
	}
};

$(document).ready(qa_thread_chat);