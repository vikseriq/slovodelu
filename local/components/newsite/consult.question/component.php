<? if (!defined('B_PROLOG_INCLUDED')) die('hard');
$site = Site::get();
$me = $site->getCurrentUser();

$section_code = arr::_($_GET, 'section');
$question_code = trim((arr::_($_GET, 'question')), '\/?');

$section = $site->getQASection($section_code);
if (!$section)
	return Site::notFound();

$q_filter = $site->getAccessFilter('question', $section['ID']);
$q_filter['CODE'] = $question_code;

$question = $site->getQuestions($q_filter, true);
if (!$question)
	return Site::notFound();

$reply = ['MESSAGE' => '', 'TYPE' => 'ERROR'];
if ($_POST['thread'] == $question['ID']){
	// handle ajax reply
	if (!$question['DISABLE_REPLY']){
		$map = ['thread', 'text', 'parent_id@reply_to'];

		if ($replyId = $site->putAnswer($me['ID'], $question['ID'], arr::mapify($map, $_POST))){
			$reply = [
				'MESSAGE' => 'Ответ успешно добавлен'
					."<script>Siteframe.scrollTo($('.dialog [data-id=".$replyId."]'));</script>",
				'TYPE' => 'OK'];
		} else {
			$reply['MESSAGE'] = $site->last_error;
		}
	} else {
		$reply['MESSAGE'] = 'Новые комментарии к этому вопросу запрещены';
	}
}

$answers = $site->getAnswers($question['ID']);

$idExperts = [];
$idUsers = [$question['AUTHOR_USER']];
foreach ($answers as $a){
	$id = $a['AUTHOR_EXPERT'];
	if ($id)
		$idExperts[] = $id;
	$id = $a['AUTHOR_USER'];
	if ($id){
		$idUsers[] = $id;
	}
}

// update lastSeen flag for current user
if ($me['ID']){
	$site->putThreadSeen($me['ID'], $question['ID']);
}

$users = $site->getUsers(array_unique($idUsers));
$experts = $site->getExperts(['ID' => array_unique($idExperts)], true);

$arResult = [
	'USERS' => $users,
	'SECTION' => $section,
	'QUESTION' => $question,
	'ANSWERS' => $answers,
	'EXPERTS' => $experts,
	'ON_REPLY' => $reply
];

$this->IncludeComponentTemplate();