<? defined('B_PROLOG_INCLUDED') || die('hard');

$sectionCode = arr::_($arParams, 'SECTION_CODE', '');
$mode = arr::_($arParams, 'MODE', 'QA');
if ($sectionCode == 'company')
	$mode = 'COMPANY';
$sections = Site::get()->getQASections();
$basePath = reset($sections)['LIST_PAGE_URL'];

if ($mode != 'COMPANY')
	array_unshift($sections, [
		'ID' => 0,
		'NAME' => 'Все категории',
		'CODE' => '',
		'SHORT_NAME' => 'Все категории',
		'SECTION_PAGE_URL' => $basePath,
		'SORT' => 1
	]);

$curSection = $sections[0];
foreach($sections as $s){
	if ($s['CODE'] == $sectionCode)
		$curSection = $s;
}

$arResult = [
	'MODE' => $mode,
	'CURRENT_SECTION' => $curSection,
	'SECTIONS' => $sections
];

$this->IncludeComponentTemplate($mode != 'COMPANY' ? '' : 'company');