<? defined('B_PROLOG_INCLUDED') || die('hard');

$segments = array_filter(explode('/', $APPLICATION->GetCurDir()));
$companyCode = array_pop($segments);
$company = reset(Site::get()->getCompanies(['CODE' => $companyCode]));

if (!$company)
	return Site::notFound();

$APPLICATION->AddChainItem('Эксперты', Site::qaExpertSectionLink());
$APPLICATION->AddChainItem('Компания');
Site::setSefParams($company);
?>
<div class="container">
	<ul class="finance-navigation bottom-border">
		<? foreach ($arResult['SECTIONS'] as $item): ?>
			<li <?=txt::cond(in_array($item['ID'], $company['QA_SECTIONS']), 'class="active"')?>>
				<a href="<?=Site::qaExpertSectionLink($item['CODE'])?>"
				   class="basic-text"><?=$item['SHORT_NAME']?></a>
			</li>
		<? endforeach ?>
	</ul>
</div>
<div class="container">
	<div class="experts-box">
		<div class="row">
			<div class="col-xs-9">
				<? $APPLICATION->IncludeComponent(
					"newsite:consult.team",
					"listing",
					array(
						'QA_COMPANY_ID' => $company['ID'],
						'EXPERT_TOTAL_ANSWERS' => true
					)
				); ?>
				<? $APPLICATION->IncludeComponent(
					"bitrix:advertising.banner",
					"raw",
					array(
						"TYPE" => "SECTION_BOTTOM",
						"CACHE_TYPE" => "A",
						"NOINDEX" => "N",
						"CACHE_TIME" => "3600",
						"QUANTITY" => "1"
					),
					false
				); ?>
			</div>
			<div class="col-xs-3">
				<div class="left-panel">
					<? $APPLICATION->IncludeComponent(
						"newsite:consult.team",
						"widget_competencies",
						array(
							'QA_COMPANY_ID' => $company['ID'],
						)
					); ?>
					<? $APPLICATION->IncludeComponent(
						"bitrix:advertising.banner",
						"raw",
						array(
							"TYPE" => "SECTION_RIGHT",
							"CACHE_TYPE" => "A",
							"NOINDEX" => "N",
							"CACHE_TIME" => "3600",
							"QUANTITY" => "1"
						),
						false
					); ?>
				</div>
			</div>
		</div>
	</div>
</div>
