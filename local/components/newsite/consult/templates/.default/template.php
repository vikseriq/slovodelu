<? defined('B_PROLOG_INCLUDED') || die('hard');

$qa_mode = $arResult['MODE'] == 'QA';
$section = $arResult['CURRENT_SECTION'];

if ($qa_mode){
	$APPLICATION->AddChainItem('Я улучшаю бизнес', reset($arResult['SECTIONS'])['SECTION_PAGE_URL']);
} else {
	$APPLICATION->AddChainItem('Бизнес-клуб', Site::qaExpertSectionLink());
}

if ($section['ID'] && $qa_mode){
	Site::setSefParams($section, true);
	$APPLICATION->AddChainItem('Вопросы');
}
?>
<div class="container">
	<h1><?=$section['NAME']?></h1>
	<ul class="finance-navigation bottom-border">
		<? foreach ($arResult['SECTIONS'] as $item): ?>
			<li <?=txt::cond($item['ID'] == $section['ID'], 'class="active"')?>>
				<a href="<?=$qa_mode ? $item['SECTION_PAGE_URL'] : Site::qaExpertSectionLink($item['CODE'])?>"
				   class="basic-text"><?=$item['SHORT_NAME']?></a>
			</li>
		<? endforeach ?>
	</ul>
</div>
<div class="container">
	<div class="experts-box">
		<? if ($qa_mode): ?>

			<div class="bottom-border">
				<a href="<?=Site::qaExpertSectionLink($section['CODE'])?>"
				   class="btn btn-link main-style-text <?=txt::cond(!$qa_mode, 'active')?>"
				   role="button">
					<h2>Эксперты</h2>
				</a>
				<a href="<?=$section['SECTION_PAGE_URL']?>"
				   class="btn btn-link main-style-text <?=txt::cond($qa_mode, 'active')?>"
				   role="button">
					<h2>Вопросы</h2>
				</a>
			</div>

			<div class="row">
				<div class="col-xs-9">
					<div class="inner">
						<? //$APPLICATION->IncludeComponent('newsite:consult.question.create'); ?>
						<?
						$filter = $section['ID'] ? ['PROPERTY_QA_SECTIONS' => [$section['ID']]] : [];
						$APPLICATION->IncludeComponent('newsite:ibface', 'faq.popular',
							[
								'DATA_TYPE' => 'ELEMENTS',
								'DATA_IBLOCK_ID' => Site::get()->scheme->faq,
								'DATA_FILTER' => $filter
							]
						); ?>
						<?
						$filter = $section['ID'] ? ['QA_SECTION_ID' => $section['ID']] : [];
						$APPLICATION->IncludeComponent('newsite:consult.question.list', 'qa', $filter); ?>
						<? $APPLICATION->IncludeComponent(
							"bitrix:advertising.banner",
							"raw",
							[
								"TYPE" => "SECTION_BOTTOM",
								"CACHE_TYPE" => "A",
								"NOINDEX" => "N",
								"CACHE_TIME" => "3600",
								"QUANTITY" => "1"
							],
							false
						); ?>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="left-panel">
						<? $APPLICATION->IncludeComponent(
							"bitrix:advertising.banner",
							"raw",
							array(
								"TYPE" => "SECTION_RIGHT",
								"CACHE_TYPE" => "A",
								"NOINDEX" => "N",
								"CACHE_TIME" => "3600",
								"QUANTITY" => "1"
							),
							false
						); ?>
						<? $APPLICATION->IncludeComponent(
							"newsite:consult.team",
							"section_brief",
							array(
								'QA_SECTION_ID' => $section['ID']
							)
						); ?>
					</div>
				</div>
			</div>
		<? else: ?>
			<div class="row">
				<div class="col-xs-9">
					<? $APPLICATION->IncludeComponent(
						"newsite:consult.team",
						"listing",
						array(
							'QA_SECTION_ID' => $section['ID'],
							'EXPERT_TOTAL_ANSWERS' => true
						)
					); ?>
					<? $APPLICATION->IncludeComponent(
						"bitrix:advertising.banner",
						"raw",
						array(
							"TYPE" => "SECTION_BOTTOM",
							"CACHE_TYPE" => "A",
							"NOINDEX" => "N",
							"CACHE_TIME" => "3600",
							"QUANTITY" => "1"
						),
						false
					); ?>
				</div>
				<div class="col-xs-3">
					<div class="left-panel">
						<? $APPLICATION->IncludeComponent(
							"newsite:consult.team",
							"widget_competencies",
							array(
								'QA_SECTION_ID' => $section['ID'],
								'AJAX_MODE' => 'Y',
								'AJAX_OPTION_JUMP' => 'N',
								'AJAX_OPTION_HISTORY' => 'N'
							)
						); ?>
						<? $APPLICATION->IncludeComponent(
							"bitrix:advertising.banner",
							"raw",
							array(
								"TYPE" => "SECTION_RIGHT",
								"CACHE_TYPE" => "A",
								"NOINDEX" => "N",
								"CACHE_TIME" => "3600",
								"QUANTITY" => "1"
							),
							false
						); ?>
					</div>
				</div>
			</div>
		<? endif ?>
	</div>
</div>
<div class="news-box">
	<div class="container">
		<? $APPLICATION->IncludeComponent(
			"newsite:ibface",
			"news.latest",
			array(
				'DATA_TYPE' => 'ELEMENTS',
				'DATA_IBLOCK_ID' => Site::get()->scheme->publications,
				'QUANTITY' => 4,
				'DATA_FILTER' => [
					'PROPERTY_QA_SECTIONS' => $section['ID']
				],
				'DATA_FETCH_PROPS' => true
			),
			false
		); ?>
	</div>
</div>