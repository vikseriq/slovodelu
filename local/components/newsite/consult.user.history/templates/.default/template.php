<ul class="my_questions-list">
	<? if ($arResult['QUESTIONS']): foreach ($arResult['QUESTIONS'] as $item):
		$answer = arr::_($arResult['ANSWERS'], $item['ID']);
		$date_value = $item['DATE_CREATE'];
		$new_answers = $item['NEW_REPLIES'];
		?>
		<li class="row question-line <?=txt::cond($new_answers, 'active')?>">
			<div class="col-xs-6 question-column">
				<span class="id-question secondary-text">
					<?=Site::qaCode($item['IBLOCK_SECTION_ID'], $item['ID'])?>
				</span>
				<a href="<?=$item['DETAIL_PAGE_URL']?>">
					<p class="question-title"><?=$item['NAME']?></p>
				</a>
			</div>

			<div class="col-xs-5 col-xs-offset-1 question-column">
				<? if (!$answer):
					if ($arResult['USER']['EXPERT']):?>
						<a href="<?=$item['DETAIL_PAGE_URL']?>">Ответить на вопрос</a>
					<? else: ?>
						<p>Ожидает ответа экспертов</p>
					<? endif ?>
				<? else :
					$date_value = $answer['DATE_CREATE'];
					$author = $answer['AUTHOR_EXPERT']
						? $arResult['EXPERTS']['EXPERTS'][$answer['AUTHOR_EXPERT']]
						: $arResult['USERS'][$answer['AUTHOR_USER']];
					$link = $author['EXPERT']
						? $arResult['EXPERTS']['COMPANIES'][$expert['COMPANY']]['DETAIL_PAGE_URL']
						: '';
					?>
					<span class="count secondary-text">
                            <span class="icon-chat-border"></span>
						<? if ($new_answers): ?>
							<b><?=$item['NEW_REPLIES']?></b>
						<? else: ?>
							<span><?=$item['ANSWERS_COUNT']?></span>
						<? endif ?>
					</span>
					<div class="img-circle" style="background-image: url('<?=$author['AVATAR']?>')"></div>
					<a <?=txt::cond($link, 'href="'.$link.'"')?>>
						<h4><?=$author['FULL_NAME']?></h4></a>
				<? endif ?>
				<time class="secondary-text text-right time" title="<?=Site::date($date_value)?>">
					<?=Site::date($date_value, 'DATE_RELATIVE')?>
				</time>
			</div>
		</li>
	<? endforeach ?>
		<!--<button class="btn btn-link btn-block bottom-border">Показать еще
			<span class="accessory-refresh"></span>
		</button>-->
	<? else: ?>
		<li class="row question-line">
			<? if ($arResult['USER']['EXPERT']): ?>
				Вам еще не задавали вопросы.
			<? else: ?>
				Вы еще не задавали вопросов.
			<? endif; ?>
		</li>
	<? endif ?>
</ul>
