<? defined('B_PROLOG_INCLUDED') || die('hard');

$site = Site::get();
$userId = arr::_($arParams, 'AUTHOR_USER');
$expertId = arr::_($arParams, 'AUTHOR_EXPERT');
$currentUser = $site->getCurrentUser();

if (!$userId && !$expertId)
	return;

if (!$expertId){
	// questions asked by user
	$qs = $site->getQuestions([
		'PROPERTY_AUTHOR_USER' => $userId
	]);
} else {
	// questions for expert reply
	$expertWork = $site->getExperts(['ID' => $expertId], true);
	$company = reset($expertWork['COMPANIES']);
	$qs = $site->getQuestions([
		'IBLOCK_SECTION_ID' => $company['QA_SECTIONS']
	]);
}

// last seen marks for threads
$threadSeen = $site->getThreadSeen($currentUser['ID']);
// if thread reply older than this date then do not count as "new reply"
$minValuableDate = strtotime($site->scheme->seen_expire);

if ($qs){
	// map questions
	$qIds = [];
	$lastAnswers = [];
	foreach ($qs as &$q){
		$qIds[] = $q['ID'];
		$q['ANSWERS_COUNT'] = 0;
		$q['NEW_REPLIES'] = 0;
		$q['LAST_REPLY'] = 0;
		$q['LAST_SEEN'] = arr::_($threadSeen, $q['ID']);
	}

	// fetching all questions' answers
	$answers = $site->getAnswers($qIds);
	foreach ($answers as $a){
		$qs[$a['QUESTION_ID']]['ANSWERS_COUNT']++;

		// find last reply time
		$qs[$a['QUESTION_ID']]['LAST_REPLY'] = max(
			$qs[$a['QUESTION_ID']]['LAST_REPLY'],
			$a['TIMESTAMP_CREATE']
		);

		// update new messages counter with expiration check
		if ($a['TIMESTAMP_CREATE'] > $qs[$a['QUESTION_ID']]['LAST_SEEN'] && $a['TIMESTAMP_CREATE'] > $minValuableDate)
			$qs[$a['QUESTION_ID']]['NEW_REPLIES']++;

		// collect all replies
		if (!isset($lastAnswers[$a['QUESTION_ID']])
			|| $lastAnswers[$a['QUESTION_ID']]['TIMESTAMP_CREATE'] < $a['TIMESTAMP_CREATE']
		){
			$lastAnswers[$a['QUESTION_ID']] = $a;
			$qs[$a['QUESTION_ID']]['LAST_REPLY'] = $a['TIMESTAMP_CREATE'];
		}
	}

	// post-filters
	if (arr::_($arParams, 'QUESTIONS_WITH_REPLY')){
		// unset q without reply
		$qs = array_filter($qs, function ($e){
			return $e['LAST_REPLY'] ? true : false;
		});
	} else if (arr::_($arParams, 'QUESTIONS_WITHOUT_REPLY')){
		// unset w with reply
		$qs = array_filter($qs, function ($e){
			return $e['LAST_REPLY'] ? false : true;
		});
	}

	// sort questions by LAST_REPLY
	usort($qs, function ($a, $b) use ($lastAnswers){
		return $b['TIMESTAMP_CREATE'] - $a['TIMESTAMP_CREATE'];
	});

	// fetching actual users
	$expertIds = [];
	$userIds = [];
	foreach ($lastAnswers as $a){
		if ($lastAnswers['AUTHOR_EXPERT'])
			$expertIds[] = $lastAnswers['AUTHOR_EXPERT'];
		else
			$userIds[] = $lastAnswers['AUTHOR_USER'];
	}

	$arResult = [
		'QUESTIONS' => $qs,
		'ANSWERS' => $lastAnswers,
		'EXPERTS' => $site->getExperts(['ID' => $expertIds], true),
		'USERS' => $site->getUsers($userIds)
	];
} else {
	$arResult = ['QUESTIONS' => []];
}

$arResult['USER'] = $currentUser;

$this->IncludeComponentTemplate();