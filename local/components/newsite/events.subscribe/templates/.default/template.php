<?
$values = $arResult['VALUES'];

if ($arResult['MESSAGE']['CODE'] == 'OK'): ?>
	<div class="message text-center text-success">Ваша заявка на регистрацию успешно зарегистрирована</div>
<? else: ?>
	<div class="row event-form">
		<script>BX.addCustomEvent('onAjaxSuccess', register_form_init);</script>
		<form method="post" action="<?=$APPLICATION->GetCurPage()?>">
			<div class="message text-danger"><?=$arResult['MESSAGE']['TEXT']?></div>
			<div class="form-group clearfix">
				<label class="col-xs-4 control-label">Данные посетителя</label>

				<div class="col-xs-5">
					<input type="text" class="form-control" name="name" placeholder="Фамилия Имя" value="<?=$values['name']?>"/>
				</div>
			</div>
			<div class="form-group clearfix">
				<label class="col-xs-4 control-label">Тип посетителя</label>

				<div class="col-xs-5">
					<div class="btn-group js-select full-width">
						<input type="hidden" name="type" value=""/>
						<button type="button" class="btn btn-dropdown dropdown-toggle full-width"
								data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="dropdown-text dropdown-value" data-value="<?=txt::ab($values['type'], 0)?>">Тип посетителя</span>
							<span class="show-on dropdown-accessory glyphicon glyphicon-chevron-down"></span>
							<span class="show-off dropdown-accessory glyphicon glyphicon-chevron-up"></span>
						</button>
						<ul class="dropdown-menu">
							<li class="basic-text"><a href="#" data-id="0">Частное лицо</a></li>
							<li class="basic-text"><a href="#" data-id="1">Представитель компании</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="js-group-1" <? if ($values['type'] == 1) echo 'style="display:none;"'; ?>>
				<div class="form-group clearfix">
					<label class="col-xs-4 control-label">Компания</label>

					<div class="col-xs-5">
						<input type="text" class="form-control" name="company_name" placeholder="Название компании" value="<?=$values['company_name']?>"/>
					</div>
				</div>
				<div class="form-group clearfix">
					<label class="col-xs-4 control-label">Должность</label>

					<div class="col-xs-5">
						<input type="text" class="form-control" name="company_position" placeholder="Занимаемая должность" value="<?=$values['company_position']?>"/>
					</div>
				</div>
			</div>
			<div class="form-group clearfix">
				<label class="col-xs-4 control-label">E-mail</label>

				<div class="col-xs-5">
					<input type="email" class="form-control" name="email" placeholder="example@mail.ru" value="<?=$values['email']?>"/>
				</div>
			</div>
			<div class="form-group clearfix">
				<label class="col-xs-4 control-label">Телефон</label>

				<div class="col-xs-5">
					<input type="tel" class="form-control" name="phone" placeholder="+375 33 555-55-55" value="<?=$values['phone']?>"/>
				</div>
			</div>

			<div class="form-group clearfix">
				<span class=" col-xs-4"></span>

				<div class="col-xs-5">
					<button class="btn btn-primary btn-block js-register">Регистрация</button>
				</div>
			</div>
		</form>
	</div>
<? endif ?>