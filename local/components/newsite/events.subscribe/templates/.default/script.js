function register_form_init(){
  $('.event-form .dropdown-menu li a').on('click', function(e){
    var base = $(this).closest('div');
    var holder = base.find('.dropdown-value');
    var id = $(this).attr('data-id');
    base.attr('data-value', id);
    holder.attr('data-id', id).text($(this).text());
    base.find('input').attr('value', id);
    base.trigger('change');
    base.find('.btn-dropdown').triggerHandler('click');
    return false;
  });
  var active_select = $('.event-form').find('.dropdown-value').attr('data-value');
  $('.event-form .dropdown-menu li a[data-id=' + active_select + ']').click();
  $('.event-form .js-select').on('change', function(){
    var id = $(this).attr('data-value');
    var action = id == 0 ? 'slideUp' : 'slideDown';
    $('.js-group-1')[action]();
  });
}

$(document).ready(register_form_init);