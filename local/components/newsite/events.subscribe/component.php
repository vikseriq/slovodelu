<?php
$site = Site::get();

$arResult['MESSAGE'] = ['TEXT' => '', 'CODE' => ''];

$event = $site->getEvent(['ID' => $arParams['EVENT_ID']]);
if (!$event)
	return;

// registration window
$reg_from = strtotime($event['REG_START']);
$reg_till = strtotime($event['REG_END']);
$now = time();

if ($now < $reg_from){
	$arResult['DATE'] = $event['REG_START'];
	$this->IncludeComponentTemplate('future');
	return;
}
if ($now > $reg_till){
	$arResult['DATE'] = $event['REG_END'];
	$this->IncludeComponentTemplate('closed');
	return;
}

$scheme = ['name', 'type', 'company_name', 'company_position', 'email', 'phone'];
$data = arr::mapify($scheme, $_POST);
$arResult['VALUES'] = $data;

if (isset($_POST['email'])){
	if ($site->putEventGuest($arParams['EVENT_ID'], $data)){
		$arResult['MESSAGE'] = ['CODE' => 'OK', 'TEXT' => 'Вы успешно подали заявку на участие'];
	} else {
		$arResult['MESSAGE'] = ['CODE' => 'ERROR', 'TEXT' => $site->last_error];
	}
}

$this->IncludeComponentTemplate();