<?
$currentSection = arr::_($arResult['SECTIONS'], $arResult['CURRENT_SECTION']);
?>
<h2>Задать вопрос эксперту</h2>
<form action="/ajax/question.php">
	<div class="message"></div>
	<div class="btn-group btn-block dropdown-base js-section">
		<input type="hidden" name="section" value="<?=txt::cond($currentSection, $currentSection['ID'], 0)?>"/>
		<button type="button" class="btn btn-dropdown dropdown-toggle btn-block"
				data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<? if ($currentSection): ?>
				<span class="dropdown-value" data-value="<?=$currentSection['ID']?>"><?=$currentSection['NAME']?></span>
			<? else: ?>
				<span class="dropdown-value" data-value="0">Выбрать раздел вопроса</span>
			<? endif ?>
			<span class="show-on dropdown-accessory glyphicon glyphicon-chevron-down"></span>
			<span class="show-off dropdown-accessory glyphicon glyphicon-chevron-up"></span>
		</button>
		<ul class="dropdown-menu">
			<? foreach ($arResult['SECTIONS'] as $item): ?>
				<li class="basic-text"><a data-id="<?=$item['ID']?>"><?=$item['NAME']?></a></li>
			<? endforeach ?>
		</ul>
	</div>
	<div class="form-group basic-text">
		<input name="title" type="text" class="form-control" placeholder="Заголовок вопроса">
	</div>
	<div class="form-group basic-text">
		<textarea name="text" class="form-control" rows="5" placeholder="Текст вопроса"></textarea>
	</div>
	<div id="settings1" class="settings-form bottom-border collapse in" aria-expanded="true">
		<h3>Настройки публикации</h3>

		<? if ($arResult['TEAMS']): ?>
			<div class="btn-group btn-block js-qe-related" data-id="<?=$currentSection['ID']?>">
				<input type="hidden" name="expert" value=""/>
				<button type="button" class="btn btn-dropdown dropdown-toggle btn-block"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="dropdown-value">Выбрать эксперта для ответа</span>
					<span class="show-on dropdown-accessory glyphicon glyphicon-chevron-down"></span>
					<span class="show-off dropdown-accessory glyphicon glyphicon-chevron-up"></span>
				</button>
				<ul class="dropdown-menu">
					<? foreach ($arResult['TEAMS']['EXPERTS'] as $item): ?>
						<li class="basic-text"><a data-id="<?=$item['ID']?>">
								<?=$item['FULL_NAME']?>
								<? if (count($arResult['TEAMS']['COMPANIES']) > 1)
									echo ' ('.$arResult['TEAMS']['COMPANIES'][$item['COMPANY']]['NAME'].')';
								?>
							</a></li>
					<? endforeach ?>
				</ul>
			</div>
		<? endif ?>

		<div class="checkbox">
			<input name="publish_smm" id="check_set_3" type="checkbox" value="1">
			<label class="basic-text" for="check_set_3">
				Разместить вопрос и ответ в Facebook в группе «Слово Делу»
			</label>
		</div>
		<div class="checkbox">
			<input name="hide_name" id="check_set_4" type="checkbox" value="1">
			<label class="basic-text" for="check_set_4">Публикоать вопрос анонимно
				(<?=Site::vendetta($arResult['CURRENT_USER']['FULL_NAME'])?>)
			</label>
		</div>
	</div>
	<div class="settings-publication">

		<a href="#settings1" class="basic-text dropdown-toggle" data-toggle="collapse"
		   aria-haspopup="true" aria-expanded="true">
			<span class="pre-accessory glyphicon glyphicon-cog"></span>
		<span class="show-on">Скрыть настройки публикации
			<span class="dropdown-accessory glyphicon glyphicon-chevron-up"></span>
		</span>
		<span class="show-off">Настройки публикации
			<span class="dropdown-accessory glyphicon glyphicon-chevron-down"></span>
		</span>
		</a>
	</div>
	<button class="btn btn-primary pull-right js-q-create" type="button">Задать вопрос</button>
</form>
<script>
	$('.dropdown-menu li a').on('click', function(){
		var base = $(this).closest('div');
		var holder = base.find('.dropdown-value');
		var id = $(this).attr('data-id');
		base.attr('data-value', id);
		holder.attr('data-id', id).text($(this).text());
		base.find('input').attr('value', id);
		base.find('.dropdown-toggle').triggerHandler('click');
		base.trigger('change');
		return false;
	});
	$('.js-section').on('change', function(){
		var id = $(this).attr('data-value');
		var eselect = $('.js-qe-related');
		var action = eselect.attr('data-id') == id ? 'slideDown' : 'slideUp';
		eselect[action]();
	});
	$('.js-q-create').on('click', function(){
		var form = $(this).closest('form');
		$.post(form.attr('action'), form.serialize())
			.done(function(data){
				form.html(data);
			})
			.fail(function(xhr){
				form.find('.message').html(xhr.responseText).show();
			});
		return false;
	});
</script>