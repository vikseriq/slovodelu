<?
$currentSection = arr::_($arResult['SECTIONS'], $arResult['CURRENT_SECTION']);
$currentExpert = reset($arResult['TEAMS']['EXPERTS']);
$expertName = $currentExpert['NAME'];
$expertName .= ' ('.reset($arResult['TEAMS']['COMPANIES'])['NAME'].')';
?>
<h2>Задать вопрос эксперту: <?=$expertName?></h2>
<form action="/ajax/question.php">
	<div class="message"></div>
	<input type="hidden" name="expert" value="<?=txt::cond($currentExpert, $currentExpert['ID'], 0)?>"/>
	<input type="hidden" name="section" value="<?=txt::cond($currentSection, $currentSection['ID'], $arResult['CURRENT_SECTION_ID'])?>"/>

	<div class="form-group basic-text">
		<input name="title" type="text" class="form-control" placeholder="Заголовок вопроса">
	</div>
	<div class="form-group basic-text">
		<textarea name="text" class="form-control" rows="5" placeholder="Текст вопроса"></textarea>
	</div>
	<div class="row">
		<div class="form-group basic-text col-sm-6">
			<input name="user_name" type="text" class="form-control" placeholder="Ваше имя">
		</div>
		<div class="form-group basic-text col-sm-6">
			<input name="user_email" type="email" class="form-control" placeholder="E-mail для получения ответа эксперта">
		</div>
	</div>

	<button class="btn btn-primary pull-right js-q-create" type="button">Задать вопрос</button>
</form>
<script>
	$('.js-q-create').on('click', function(){
		var form = $(this).closest('form');
		$.post(form.attr('action'), form.serialize())
			.done(function(data){
				form.html(data);
			})
			.fail(function(xhr){
				form.find('.message').html(xhr.responseText).show();
			});
		return false;
	});
</script>