<? if (!defined('B_PROLOG_INCLUDED')) die('hard');
$site = Site::get();
$sections = $site->getQASections();

$site->allowAnon = arr::_($arParams, 'ANON') ? true : false;

$me = $site->getCurrentUser();

if (!$site->allowAnon && (!$me['ID'] || $me['EXPERT']))
	return $this->IncludeComponentTemplate('onlyuser');

if (isset($_POST['text'])){
	Site::reset();
	// process input data
	$map = ['section', 'expert', 'name@title', 'text', 'smm@publish_smm', 'privacy@hide_name', 'user_name', 'user_email'];
	$data = arr::mapify($map, $_POST);

	if ($site->putQuestion($me['ID'], $data)){
		$this->IncludeComponentTemplate('success');
	} else {
		$arResult['MESSAGE'] = $site->last_error;
		$this->IncludeComponentTemplate('error');
	}

	exit;
}

$expert_id = arr::_($arParams, 'EXPERT_ID');

$arResult = [
	'CURRENT_USER' => $me,
	'SECTIONS' => $sections,
	'CURRENT_SECTION' => arr::_($arParams, 'SECTION_CODE'),
	'CURRENT_SECTION_ID' => arr::_($arParams, 'SECTION_ID'),
	'CURRENT_EXPERT_ID' => $expert_id,
	'TEAMS' => []
];

if ($expert_id){
	// direct question
	$arResult['TEAMS'] = $site->getExperts(['ID' => $expert_id], true);
} else {
	// mass question
	$current_section = arr::_($arResult['SECTIONS'], $arResult['CURRENT_SECTION']);
	if ($current_section){
		$arResult['TEAMS'] = $site->getTeams(['COMPANIES' => ['PROPERTY_QA_SECTIONS' => $current_section['ID']]]);
	}
}

$this->IncludeComponentTemplate();