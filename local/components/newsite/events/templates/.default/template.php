<?php
$event = $arResult['EVENT'];

Site::setSefParams($event);
$APPLICATION->AddChainItem('Мероприятия', '/events/');
$APPLICATION->AddChainItem(NAVCHAIN_BACK.' К списку мероприятий', '/events/');

$coords = explode(',', $event['COORD']);
$mapPoint = [
	'yandex_lat' => $coords[0],
	'yandex_lon' => $coords[1],
	'yandex_scale' => '15',
	'PLACEMARKS' => [[
		'LAT' => $coords[0],
		'LON' => $coords[1],
		'TEXT' => htmlspecialchars($event['ADDRESS'])
	]]
];

?>
<div class="carousel">
	<div class="carousel-inner">
		<div class="item active">
			<img class="first-slide" src="<?=CFile::GetPath($event['BANNER'])?>">

			<div class="container">
				<div class="carousel-caption">
					<h1><?=$event['PREVIEW_TEXT']?></h1>
					<button class="btn btn-primary js-go-reg">Регистрация на мероприятие</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="page-white">
	<div class="bb">
		<?=$event['DETAIL_TEXT']?>
	</div>
	<div class="event-subscribe bb">
		<h2>Регистрация</h2>
		<? $APPLICATION->IncludeComponent(
			"newsite:events.subscribe", '', [
				'EVENT_ID' => $event['ID'],
				'AJAX_MODE' => 'Y',
				'AJAX_OPTION_JUMP' => 'N',
				'AJAX_OPTION_HISTORY' => 'N',
			]
		); ?>
	</div>
	<div class="bb">
		<h2>Программа мероприятия</h2>

		<div class="schedule-template">
			<ul>
			<? foreach($event['SCHEDULE'] as $check): ?>
				<li>
					<span><?=$check['TIME']?></span>
					<span><?=html_entity_decode($check['DESC'])?></span>
				</li>
			<? endforeach ?>
			</ul>
		</div>
	</div>
	<div>
		<h2>Место проведения</h2>
		<?=$event['ADDRESS']?>
	</div>
</div>
<div class="margin-bottom"><? $APPLICATION->IncludeComponent(
		"bitrix:map.yandex.view",
		"",
		Array(
			"COMPONENT_TEMPLATE" => ".default",
			"CONTROLS" => array("ZOOM"),
			"INIT_MAP_TYPE" => "MAP",
			"MAP_DATA" => serialize($mapPoint),
			"MAP_HEIGHT" => "300",
			"MAP_ID" => "",
			"MAP_WIDTH" => "100%",
			"OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING")
		)
	); ?>
</div>
<script>
	$(document).ready(function(){
		$('.js-go-reg').click(function(){
			Siteframe.scrollTo($('.event-subscribe'));
			return false;
		});
	})
</script>