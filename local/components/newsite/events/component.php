<? defined('B_PROLOG_INCLUDED') || die('hard');
$site = Site::get();

$eventCode = $arParams['EVENT_CODE'];
$event = $site->getEvent($eventCode);

if (!$event)
	return Site::notFound();

$arResult['EVENT'] = $event;

$this->IncludeComponentTemplate();