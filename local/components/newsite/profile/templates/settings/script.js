$(document).ready(function(){
	$('.js-image-upload').click(function(e){
		e.preventDefault();
		$(this).closest('div').find('input').trigger('click');
	});
	$('.js-image-placeholder').change(function(){
		var files = this.files;
		if (!files.length)
			return;
		var target = $(this).attr('data-target');
		var fr = new FileReader();
		fr.onloadend = function(e){
			$(target).css('background-image', 'url(' + e.target.result + ')');
		};
		fr.readAsDataURL(files[0]);
	});
});