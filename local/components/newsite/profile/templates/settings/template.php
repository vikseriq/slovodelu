<?
$user = $arResult['USER'];
$bxUser = $arResult['BX_USER'];
?>
<h1><?=$user['FULL_NAME']?></h1>

<div class="row">
	<div class="col-xs-9">
		<div class="profile-settings bottom-border">
			<form action="<?=$APPLICATION->GetCurUri()?>" method="post" enctype="multipart/form-data">
				<div class="box-title bottom-border">
					<h2>Настройки профиля</h2>
					<? if (isset($arResult['MESSAGE'])): ?>
						<p class="text-<?=txt::cond($arResult['MESSAGE']['TYPE'] == 'ERROR', 'danger', 'success')?>">
							<?=$arResult['MESSAGE']['MESSAGE']?>
						</p>
					<? endif ?>
				</div>

				<div class="profile-settings-form">
					<h3>Контактная информация</h3>

					<div class="form-horizontal padding-bottom">
						<div class="form-group">
							<label class="col-xs-4 control-label">Контактное лицо</label>

							<div class="col-xs-5">
								<input type="text" class="form-control" name="USER[NAME]"
									   value="<?=$bxUser['NAME']?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">Фамилия</label>

							<div class="col-xs-5">
								<input type="text" class="form-control" name="USER[LAST_NAME]"
									   value="<?=$bxUser['LAST_NAME']?>" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">E-mail</label>

							<div class="col-xs-5">
								<input type="email" class="form-control" name="USER[EMAIL]"
									   value="<?=$bxUser['EMAIL']?>" placeholder="expert@company.com">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">Телефон</label>

							<div class="col-xs-5">
								<input type="tel" class="form-control" name="USER[PHONE]"
									   value="<?=$bxUser['WORK_PHONE']?>" placeholder="+375 29 555-55-55">
							</div>
						</div>
						<div class="profile-image">
									<span class="img-circle" id="profile-avatar"
										  style="background-image: url('<?=$user['AVATAR']?>');">
										<span class="add-image <?
										echo txt::cond($user['AVATAR'] != Site::get()->scheme->placeholder_avatar, 'active')
										?> js-image-upload">+</span>
									</span>
							<input type="file" class="js-image-placeholder" data-target="#profile-avatar"
								   name="USER_AVATAR"/>
						</div>
					</div>

					<h3>Смена пароля</h3>

					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-xs-4 control-label">Новый пароль</label>

							<div class="col-xs-5">
								<input type="password" name="PASSWORD[NEW]" class="form-control" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">Повторите пароль</label>

							<div class="col-xs-5">
								<input type="password" name="PASSWORD[REPEAT]" class="form-control" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<span class=" col-xs-4"></span>

							<div class="col-xs-5">
								<button class="btn btn-primary btn-block">Изменить данные</button>
							</div>
						</div>
					</div>

					<h3>Информация</h3>

					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-xs-4 control-label">Дата регистрации</label>

							<div class="col-xs-5">
									<span
										class="form-control-static"><?=Site::date($bxUser['DATE_REGISTER'], 'FULL_RELATIVE')?></span>
							</div>
						</div>
						<? if ($user['EXPERT']): ?>
							<div class="form-group">
								<label class="col-xs-4 control-label">Компания</label>

								<div class="col-xs-5">
									<div class="form-control-static">
										<div><img src="<?=$user['COMPANY']['LOGO']?>"/></div>

										<a href="<?=$user['COMPANY']['DETAIL_PAGE_URL']?>"><?=$user['COMPANY']['NAME']?></a>
									</div>
								</div>
							</div>
						<? endif ?>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="col-xs-3">
		<div class="left-panel">
			<? $APPLICATION->IncludeComponent(
				"bitrix:advertising.banner",
				"raw",
				array(
					"TYPE" => "SECTION_RIGHT",
					"CACHE_TYPE" => "A",
					"NOINDEX" => "N",
					"CACHE_TIME" => "3600",
					"QUANTITY" => "1"
				),
				false
			); ?>
		</div>
	</div>
</div>