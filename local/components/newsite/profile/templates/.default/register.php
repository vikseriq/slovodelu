<div class="profile-settings bottom-border">
	<div class="box-title bottom-border">
		<h2>Регистрация</h2>
	</div>
	<div class="profile-settings-form">
		<form name="system_register_form" class="form-horizontal padding-bottom" method="POST"
			  action="<?=$arResult["AUTH_URL"]?>">
			<? if ($arResult["BACKURL"]): ?>
				<input type="hidden" name="BACKURL" value="<?=$arResult["BACKURL"]?>"/>
			<? endif ?>
			<? foreach ($arResult["POST"] as $key => $value): ?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>"/>
			<? endforeach ?>
			<input type="hidden" name="AUTH_FORM" value="Y"/>
			<input type="hidden" name="TYPE" value="REGISTRATION"/>

			<div class="form-group">
				<label class="col-xs-4 control-label">E-mail</label>

				<div class="col-xs-5">
					<input type="text" class="form-control" name="USER_EMAIL" placeholder="user@example.com"
						   value="<?=$arResult["USER_EMAIL"]?>"/>
					<input type="hidden" name="USER_LOGIN" value=""/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-4 control-label">Пароль</label>

				<div class="col-xs-5">
					<input type="password" class="form-control" name="USER_PASSWORD" placeholder="password"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-4 control-label">Подтверждение пароля</label>

				<div class="col-xs-5">
					<input type="password" class="form-control" name="USER_CONFIRM_PASSWORD" placeholder="password"/>
				</div>
			</div>
			<? if ($arResult["CAPTCHA_CODE"]): ?>
				<div class="form-group">
					<label class="col-xs-4 control-label">Проверочный код</label>

					<div class="col-xs-5">
						<div class="row">
							<div class="col-xs-6">
									<span class="form-control-static">
									<img
										src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>"/>
									</span>

							</div>
							<div class="col-xs-6">
								<input type="hidden" name="captcha_sid"
									   value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>

								<input type="text" class="form-control" name="captcha_word" maxlength="50"
									   value=""/>

							</div>
						</div>
					</div>
				</div>
			<? endif ?>

			<div class="form-group">
				<span class=" col-xs-4"></span>

				<div class="col-xs-5">
					<? if ($arResult['MESSAGE']['TYPE'] == 'ERROR' && isset($_POST['USER_LOGIN']))
						echo '<p class="text-danger">'.htmlspecialchars_decode($arResult['MESSAGE']['MESSAGE']).'</p>';
					?>
					<button class="btn btn-primary btn-block">Создать профиль</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	document.system_register_form.onsubmit = function(){
		this.USER_LOGIN.value = this.USER_EMAIL.value;
	}
</script>