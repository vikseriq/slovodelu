<div class="profile">
	<div class="profile-settings bottom-border">
		<div class="box-title bottom-border">
			<h2>Восстановление пароля</h2>
		</div>
		<?
		if ($arResult['MESSAGE']['TYPE'] == 'OK'): ?>
			<p class="text-success"><?=$arResult['MESSAGE']['MESSAGE']?></p>
		<? else: ?>
			<div class="profile-settings-form">
				<form name="system_forgot_form" class="form-horizontal padding-bottom" method="POST"
					  action="<?=$arResult["AUTH_URL"]?>">
					<? if ($arResult["BACKURL"]): ?>
						<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>"/>
					<? endif ?>
					<input type="hidden" name="AUTH_FORM" value="Y"/>
					<input type="hidden" name="TYPE" value="SEND_PWD">

					<? if ($arResult['MESSAGE']['MESSAGE'] && isset($_POST['USER_EMAIL'])):
						echo '<p class="text-danger">'.htmlspecialchars_decode($arResult['MESSAGE']['MESSAGE']).'</p>';
					endif ?>

					<div class="form-group">
						<label class="col-xs-4 control-label">E-mail</label>

						<div class="col-xs-5">
							<input type="text" class="form-control" name="USER_NAME"
								   placeholder="user@example.com" value="<?=$arResult["LAST_LOGIN"]?>"/>
							<input type="hidden" name="USER_EMAIL" value=""/>
						</div>
					</div>

					<div class="form-group">
						<span class=" col-xs-4"></span>

						<div class="col-xs-5">
							<button class="btn btn-primary btn-block">Выслать письмо</button>
						</div>
					</div>
				</form>
			</div>
		<? endif ?>
	</div>
</div>
<script>
	var form = document.system_forgot_form;
	form.onsubmit = function(){
		this.USER_EMAIL.value = this.USER_NAME.value;
	};
	form.USER_NAME.focus();
</script>