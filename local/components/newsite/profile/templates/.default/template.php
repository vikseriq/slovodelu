<h1><?=$arResult['USER']['FULL_NAME']?>
	<a href="/personal/settings/" class="basic-text">
		<span class="pre-accessory glyphicon glyphicon-cog"></span>Настройки профиля
	</a>
</h1>

<? if ($arResult['USER']['EXPERT']): ?>
	<div class="my_questions">
		<div class="box-title bottom-border">
			<h2>Новые вопросы</h2>
		</div>
		<? $APPLICATION->IncludeComponent('newsite:consult.user.history', '',
			[
				'AUTHOR_EXPERT' => $arResult['USER']['EXPERT']['ID'],
				'QUESTIONS_WITHOUT_REPLY' => true
			]); ?>
	</div>

	<div class="my_questions">
		<div class="box-title bottom-border">
			<h2>Активные вопросы</h2>
		</div>
		<? $APPLICATION->IncludeComponent('newsite:consult.user.history', '',
			[
				'AUTHOR_EXPERT' => $arResult['USER']['EXPERT']['ID'],
				'QUESTIONS_WITH_REPLY' => true
			]); ?>
	</div>
<? else: ?>
	<div class="my_questions">
		<div class="box-title bottom-border">
			<h2>Активные вопросы</h2>
		</div>
		<? $APPLICATION->IncludeComponent('newsite:consult.user.history', '',
			['AUTHOR_USER' => $arResult['USER']['ID']]
		); ?>
	</div>
<? endif ?>