<div class="profile-settings bottom-border">
	<div class="box-title bottom-border">
		<h2>Авторизация</h2>
	</div>
	<div class="profile-settings-form">
		<form name="system_auth_form<?=$arResult["RND"]?>" class="form-horizontal padding-bottom" method="POST"
			  action="<?=$arResult["AUTH_URL"]?>">
			<? if ($arResult["BACKURL"]): ?>
				<input type="hidden" name="BACKURL" value="<?=$arResult["BACKURL"]?>"/>
			<? endif ?>
			<? foreach ($arResult["POST"] as $key => $value): ?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>"/>
			<? endforeach ?>
			<input type="hidden" name="AUTH_FORM" value="Y"/>
			<input type="hidden" name="TYPE" value="AUTH"/>

			<div class="form-group">
				<label class="col-xs-4 control-label">E-mail/логин</label>

				<div class="col-xs-5">
					<input type="text" class="form-control" name="USER_LOGIN" placeholder="user@example.com"
						   value="<?=$arResult["LAST_LOGIN"]?>"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-4 control-label">Пароль</label>

				<div class="col-xs-5">
					<input type="password" class="form-control" name="USER_PASSWORD" placeholder="password"/>
				</div>
			</div>
			<? if ($arResult["CAPTCHA_CODE"]): ?>
				<div class="form-group">
					<label class="col-xs-4 control-label">Проверочный код</label>

					<div class="col-xs-5">
						<div class="row">
							<div class="col-xs-6">
									<span class="form-control-static">
									<img
										src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>"/>
									</span>

							</div>
							<div class="col-xs-6">
								<input type="hidden" name="captcha_sid"
									   value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>

								<input type="text" class="form-control" name="captcha_word" maxlength="50"
									   value=""/>

							</div>
						</div>
					</div>
				</div>
			<? endif ?>

			<div class="form-group">
				<span class=" col-xs-4"></span>

				<div class="col-xs-5">
					<? if ($arResult['MESSAGE']['TYPE'] == 'ERROR' && isset($_POST['USER_LOGIN']))
						echo '<p class="text-danger">'.htmlspecialchars_decode($arResult['MESSAGE']['MESSAGE']).'</p>';
					?>
					<button class="btn btn-primary btn-block">Войти</button>
					<br/>
					<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="text-muted">Забыли пароль?</a> |
					<a href="<?=$arResult["AUTH_REGISTER_URL"]?>" class="text-primary">Регистрация</a>
				</div>
			</div>
			<div class="form-group">
				<label class="col-xs-4 control-label">Вход через социальные сети</label>

				<div class="col-xs-5">
					<? if ($arResult["AUTH_SERVICES"]): ?>
						<?
						$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "flat",
							array(
								"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
								"CURRENT_SERVICE" => $arResult["CURRENT_SERVICE"],
								"AUTH_URL" => $arResult["AUTH_URL"],
								"AJAX_MODE" => "Y",
								"POST" => $arResult["POST"],
								"SHOW_TITLES" => $arResult["FOR_INTRANET"] ? 'N' : 'Y',
								"FOR_SPLIT" => $arResult["FOR_INTRANET"] ? 'Y' : 'N',
								"AUTH_LINE" => $arResult["FOR_INTRANET"] ? 'N' : 'Y',
							),
							$component,
							array()
						);
						?>
					<? endif ?>
				</div>
			</div>
		</form>
	</div>
</div>