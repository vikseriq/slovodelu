<? defined('B_PROLOG_INCLUDED') || die('hard');

if (Site::$isAjax)
	$APPLICATION->RestartBuffer();

$site = Site::get();
$mode = arr::_($arParams, 'MODE', 'PROFILE');
$arResult = arr::_($arParams, '_RESULT', []);
$user = $site->getCurrentUser();

if ($mode == 'FORGOT' || isset($_GET['forgot_password'])){
	$this->IncludeComponentTemplate('forgot');
} else if ($mode == 'REGISTER' || isset($_GET['register'])){
	if ($user['ID']){
		LocalRedirect('/personal/settings/');
		exit;
	}
	$this->IncludeComponentTemplate('register');
} else if ($mode == 'LOGIN' || !$user['ID']){
	return $this->IncludeComponentTemplate('login');
} else {
	$arResult['MESSAGE'] = arr::_($_SESSION, '_PROFILE_MESSAGE');
	unset($_SESSION['_PROFILE_MESSAGE']);

	if ($componentTemplate == 'settings' && $new_fields = arr::_($_POST, 'USER')){
		$error = [];
		$cuser = new CUser;
		$map = ['NAME' => 'NAME', 'LAST_NAME' => 'LAST_NAME',
			'EMAIL' => 'EMAIL', 'LOGIN' => 'EMAIL', 'WORK_PHONE' => 'PHONE'];
		$fields = [];

		$passwd = arr::_($_POST, 'PASSWORD');
		if ($passwd['NEW']){
			if ($passwd['NEW'] != $passwd['REPEAT'])
				$error[] = 'Пароли не совпадают';
			if (!$error)
				$fields['PASSWORD'] = $passwd['NEW'];
		}

		foreach ($map as $k_to => $k_from){
			if ($v = arr::_($new_fields, $k_from))
				$fields[$k_to] = $v;
		}
		if (!$fields['NAME'] || !$fields['LAST_NAME'])
			$error[] = 'Пожалуйста, представьтесь';
		if (!filter_var($fields['EMAIL'], FILTER_VALIDATE_EMAIL))
			$error[] = 'Введите корректный email-адрес';

		$file = arr::_($_FILES, 'USER_AVATAR');
		if ($file && $file['error'] == 0){
			$file['old_file'] = $fields['PERSONAL_PHOTO'];
			$fields['PERSONAL_PHOTO'] = $file;
		}

		if (!$error){
			if (!$cuser->Update($user['ID'], $fields))
				$error[] = $cuser->LAST_ERROR;
		}

		$arResult['MESSAGE'] = [
			'TYPE' => count($error) ? 'ERROR' : 'OK',
			'MESSAGE' => count($error) ? implode("\n", $error) : 'Профиль успешно обновлен'
		];

		if ($arResult['MESSAGE']['TYPE'] == 'OK'){
			$_SESSION['_PROFILE_MESSAGE'] = $arResult['MESSAGE'];
			LocalRedirect($APPLICATION->GetCurUri());
			exit;
		}
	}

	$bxUser = CUser::GetByID($user['ID'])->Fetch();

	// sync expert profile image with user profile image
	if ($componentTemplate == 'settings' && $user['EXPERT']){
		$ob = CIBlockElement::GetByID($user['EXPERT']['ID']);
		if ($ar = $ob->GetNext()){
			if ($ar['PREVIEW_PICTURE'] != $bxUser['PERSONAL_PHOTO']){
				// ACHTUNG! Street magic!
				$DB->Query("UPDATE b_iblock_element SET PREVIEW_PICTURE = '".$bxUser['PERSONAL_PHOTO']."'
					WHERE ID = '".$user['EXPERT']['ID']."'", true, __LINE__);
				// ~Street magic
				$user = $site->getCurrentUser(true);
			}
		}
	}

	$arResult['USER'] = $user;
	$arResult['BX_USER'] = $bxUser;

	$this->IncludeComponentTemplate();
}

if (Site::$isAjax)
	exit;