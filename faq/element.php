<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$elementCode = arr::_($_GET, 'element');
if ($elementCode && $_p = strpos($elementCode, '/'))
	$elementCode = substr($elementCode, 0, $_p);

if ($elementCode):
	$APPLICATION->IncludeComponent('newsite:ibface', 'faq.item',
		[
			'DATA_TYPE' => 'ELEMENTS',
			'DATA_IBLOCK_ID' => Site::get()->scheme->faq,
			'DATA_FILTER' => [
				'CODE' => $elementCode,
			]
		]);
endif;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>
